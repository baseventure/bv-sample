package com.fnfis.sample.core.notifications;

import org.simplejavamail.email.Email;
import org.simplejavamail.mailer.Mailer;

import com.google.common.util.concurrent.RateLimiter;

/**
 * A @{link MailServer} wrapped around a simplemail @{link Mailer}
 * <br/>
 * This wrapper provides a means of rate limiting the email sends down to the underlying
 * Simplemail mailer.
 */
public class MailerMailSender implements MailSender {

	private Mailer mailer;
	private int rateLimit;		// Per second

	// CONSTRUCTORS

	public MailerMailSender(Mailer mailer, int rateLimit) {
		super();
		setMailer(mailer);
		setRateLimit(rateLimit);
	}

	// GETTERS/SETTERS

	protected Mailer getMailer() { return mailer; }
	protected void setMailer(Mailer mailer) { this.mailer = mailer; }

	/**
	 * The rate limit (per second) for sending notifications to the mailer.
	 */
	protected int getRateLimit() { return rateLimit; }
	protected void setRateLimit(int rateLimit) { this.rateLimit = rateLimit; }

	// DERIVED

	private RateLimiter rateLimiter;

	protected RateLimiter getRateLimiter() {
		if (rateLimiter == null) {
			rateLimiter = RateLimiter.create(getRateLimit());
		}
		return rateLimiter;
	}

	// REQUIRED

	@Override
	public boolean validate(Email email) {
		return getMailer().validate(email);
	}

	@Override
	public void sendMail(Email email) {
		getRateLimiter().acquire();
		getMailer().sendMail(email);
	}
}
