package com.fnfis.sample.core.model.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DateUtils {
	
	public static long MIN_INSTANT = LocalDateTime.MIN.toEpochSecond(ZoneOffset.UTC);
	public static long MAX_INSTANT = LocalDateTime.MAX.toEpochSecond(ZoneOffset.UTC);
	
	private static final LocalTime MAX_SECONDS = LocalTime.of(23, 59, 59, 0); 
	
	// TO DATE
	
	/**
	 * Answer a {@link Date} for the start of the specified year (in UTC).
	 * The start is considered to be midnight on the first day of the first month of that year
	 */
	public static Date startOf(int year) {
		return startOf(LocalDate.of(year, Month.JANUARY, 1));
	}
	
	/**
	 * Answer a {@link Date} for the end of the specified year (in UTC).
	 * The end is considered to be 11:59:... on the last day of the last month of that year
	 */
	public static Date endOf(int year) {
		return endOf(YearMonth.of(year, Month.DECEMBER).atEndOfMonth());
	}
	
	/**
	 * Answer a {@link Date} for the start of the specified year (in UTC).
	 * The start is considered to be midnight on the first day of the first month of that year
	 */
	public static Date startOf(Year year) {
		return startOf(year.getValue());
	}
	
	/**
	 * Answer a {@link Date} for the end of the specified year (in UTC).
	 * The end is considered to be 11:59:... on the last day of the last month of that year
	 */
	public static Date endOf(Year year) {
		return endOf(year.getValue());
	}
	
	/**
	 * Answer a {@link Date} for the start of the specified year and month (in UTC).
	 * The start is considered to be midnight on the first day of that year/month
	 */
	public static Date startOf(YearMonth yearMonth) {
		return startOf(yearMonth.getYear(), yearMonth.getMonth());
	}
	
	/**
	 * Answer a {@link Date} for the end of the specified year and month (in UTC).
	 * The end is considered to be 11:59:... on the last day of that year/month
	 */
	public static Date endOf(YearMonth yearMonth) {
		return endOf(yearMonth.getYear(), yearMonth.getMonth());
	}
	
	/**
	 * Answer a {@link Date} for the start of the specified year and month (in UTC).
	 * The start is considered to be midnight on the first day of that year/month
	 */
	public static Date startOf(int year, Month month) {
		return startOf(LocalDate.of(year, month, 1));
	}
	
	/**
	 * Answer a {@link Date} for the end of the specified year and month (in UTC).
	 * The end is considered to be 11:59:... on the last day of that year/month
	 */
	public static Date endOf(int year, Month month) {
		return endOf(YearMonth.of(year, month).atEndOfMonth());
	}
	
	/**
	 * Answer a {@link Date} for the start of the specified year, month and day (in UTC).
	 * The start is considered to be midnight on that year/month/day
	 */
	public static Date startOf(YearMonth yearMonth, int dayOfMonth) {
		return startOf(yearMonth.getYear(), yearMonth.getMonth(), dayOfMonth);
	}

	/**
	 * Answer a {@link Date} for the end of the specified year, month and day (in UTC).
	 * The end is considered to be 11:59:... on that year/month/day
	 */
	public static Date endOf(YearMonth yearMonth, int dayOfMonth) {
		return endOf(yearMonth.getYear(), yearMonth.getMonth(), dayOfMonth);
	}
	
	/**
	 * Answer a {@link Date} for the start of the specified year, month and day (in UTC).
	 * The start is considered to be midnight on that year/month/day
	 */
	public static Date startOf(int year, Month month, int dayOfMonth) {
		return startOf(LocalDate.of(year, month, dayOfMonth));
	}

	/**
	 * Answer a {@link Date} for the end of the specified year, month and day (in UTC).
	 * The end is considered to be 11:59:... on that year/month/day
	 */
	public static Date endOf(int year, Month month, int dayOfMonth) {
		return endOf(LocalDate.of(year, month, dayOfMonth));
	}
	
	/**
	 * Answer a {@link Date} for the start of the specified {@link LocalDate} (in UTC).
	 * The start is considered to be midnight on that year/month/day
	 */
	public static Date startOf(LocalDate localDate) {
		return asUTC(localDate.atStartOfDay());
	}
	
	/**
	 * Answer a {@link Date} for the end of the specified {@link LocalDate} (in UTC).
	 * The end is considered to be 11:59:... on that year/month/day
	 */
	public static Date endOf(LocalDate localDate) {
		// return asUTC(localDate.atTime(LocalTime.MAX));
		return asUTC(localDate.atTime(MAX_SECONDS));
	}
	
	/**
	 * Answer a {@link Date} for the specified {@link LocalDateTime} (in UTC).
	 */
	public static Date asUTC(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneOffset.UTC).toInstant());
	}
	
	// FROM DATE
	
	/**
	 * Answer the specified (UTC) date as a {@link LocalDateTime}.
	 */
	public static LocalDateTime asLocalDateTime(Date date) {
		return date.toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
	}

	/**
	 * Answer the specified (UTC) date as a {@link LocalDate}.
	 * Note that this is a 'lossy' transformation in that time information is not preserved.
	 */
	public static LocalDate asLocalDate(Date date) {
		return asLocalDateTime(date).toLocalDate();
	}

	/**
	 * Answer the specified (UTC) date as a {@link YearMonth}.
	 * Note that this is a 'lossy' transformation in that day and time information is not preserved.
	 */
	public static YearMonth asYearMonth(Date date) {
		LocalDate localDate = asLocalDate(date);
		return YearMonth.of(localDate.getYear(), localDate.getMonth());
	}
	
	/**
	 * Answer the specified (UTC) date as a {@link Year}.
	 * Note that this is a 'lossy' transformation in that month, day and time information is not preserved.
	 */
	public static Year asYear(Date date) {
		YearMonth yearMonth = asYearMonth(date);
		return Year.of(yearMonth.getYear());
	}
	
	/**
	 * Generate a series of {@link Date}s between the start and end date (inclusive) using the
	 * specified increment and precision.  If unspecified, increment/precision are assumed to be 1 day.
	 * If the increment is negative, then the series will be generate starting from the end date back
	 * to the start date.  In either case, the resulting dates will be returned in ascending order.
	 */
	public static List<Date> generateDates(Date startDate, Date endDate, Integer increment, Integer precision) {
		List<Date> dates = new ArrayList<>();
		if (startDate != null && endDate != null) {
			increment = (increment == null) ? 1 : increment;
			precision = (precision == null) ? Calendar.DAY_OF_MONTH : precision;
			
			boolean decrement = (increment < 0);
			Calendar begin = Calendar.getInstance();
			begin.setTime(decrement ? endDate : startDate);
			Calendar finish = Calendar.getInstance();
			finish.setTime(decrement ? startDate : endDate);
			
			boolean finished = false;
			Calendar current = (Calendar) begin.clone();
			while (!finished) {
				dates.add(current.getTime());
				current.add(precision, increment);
				finished = decrement
					? current.getTimeInMillis() < finish.getTimeInMillis()
					: current.getTimeInMillis() > finish.getTimeInMillis();
			}
			// Ensure that the finish date is included
			if (dates.get(dates.size() - 1).getTime() != finish.getTimeInMillis()) {
				dates.add(finish.getTime());
			}
			if (decrement) {
				Collections.reverse(dates);
			}
		}
		
		return dates;
	}
	
	/**
	 * Generate a series of timess between the start and end time (inclusive) using the
	 * specified increment and precision.  If unspecified, increment/precision are assumed to be 1 day.
	 * If the increment is negative, then the series will be generate starting from the end date back
	 * to the start date.  In either case, the resulting times will be returned in ascending order.
	 */
	public static List<Long> generateTimes(long startTime, long endTime, Integer increment, Integer precision) {
		return generateDates(new Date(startTime), new Date(endTime), increment, precision).stream()
			.map((date) -> date.getTime())
			.collect(Collectors.toList());
	}
}
