package com.fnfis.sample.core.notifications;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubstitutionUtils {
	private static final Logger log = LoggerFactory.getLogger(SubstitutionUtils.class);

	// Prevent instantiation
	private SubstitutionUtils() { super(); }

	/**
	 * Attempt to guess the component type for a type-erased collection.
	 * This will iterate over all the objects in the collection, and find the lowest
	 * common supertype among all objects
	 */
	protected static Class<?> guessComponentType(Collection<?> collection) {
		Class<?> guess = null;
		for (Object o : collection) {
			guess = (guess == null || guess == o.getClass())
				? o.getClass()
				: lowestCommonSuper(guess, o.getClass());
		}
		return guess;
	}

	/**
	 * Return the lowest common supertype between two specified types.
	 */
	protected static Class<?> lowestCommonSuper(Class<?> c1, Class<?> c2) {
		Set<Class<?>> c1Supers = supersFor(c1);
		while(!c1Supers.contains(c2)) {
			c2 = c2.getSuperclass();
		}
		return c2;
	}

	/**
	 * Answer all the supertypes for the specified type;
	 */
	protected static Set<Class<?>> supersFor(Class<?> c) {
		if (c == null) return new HashSet<>();

		Set<Class<?>> supers = supersFor(c.getSuperclass());
		supers.add(c);
		return supers;
	}

	// MATCHING

	/**
	 * Answer a boolean indicating if two types are the same
	 */
	public static boolean typeMatches(Object s1, Object s2) {
		return (s1 == s2 || (s1 != null && s2 != null && s1.getClass() == s2.getClass()));
	}

	// MERGING

	/**
	 * Merge two maps.  Merging will apply all the K/V entries from the addition map into the base map.
	 * In general, merging will replace entries on key collisions.  There are two special cases:
	 *
	 * <ul>
	 * <li>Collection/Array: if both values are collection/array, merge together, preventing duplicates</li>
	 * <li>Map: if both values are maps, merge together recursively
	 * </ul>
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <K, V> void mergeMaps(Map<K, V> base, Map<K, V> additions) {
		for (Map.Entry<K, V> entry : additions.entrySet()) {
			// Cannot merge null -- ignore
			if (entry.getValue() == null) {
				log.warn(String.format("Attempting to merge entry '%s' with null value", entry.getKey()));
			}
			else {
				base.merge(entry.getKey(), entry.getValue(), (baseV, additionV) -> {

					// Null <- (Non-null): replace an existing null value with whatever the addition is
					if (baseV == null) {
						return additionV;
					}

					// Array <- Array
					if (baseV.getClass().isArray() && additionV.getClass().isArray()
						&& baseV.getClass().getComponentType().isAssignableFrom(additionV.getClass().getComponentType())) {
						try {
							return (V) mergeArray(baseV.getClass().getComponentType(), (Object[]) baseV, (Object[]) additionV);
						} catch (ClassCastException e) {
							// Cannot merge, so just replace
							log.info(String.format("Unable to merge arrays for '%s' (mismatched component type); replacing instead", entry.getKey()));
							return additionV;
						}
					}

					// Collection <- Collection
					if (baseV instanceof Collection<?> && additionV instanceof Collection<?>) {
						try {
							mergeCollection(guessComponentType((Collection) baseV), (Collection) baseV, (Collection) additionV);
							return baseV;
						} catch (ClassCastException e) {
							// Type mismatch, so just replace
							log.info(String.format("Unable to merge collections for '%s' (mismatched component type); replacing instead", entry.getKey()));
							return additionV;
						}
					}
					// Array <- Collection
					if (baseV instanceof Collection<?> && additionV.getClass().isArray()) {
						try {
							mergeCollection(guessComponentType((Collection) baseV), (Collection) baseV, (Object[]) additionV);
							return baseV;
						} catch (ClassCastException e) {
							// Type mismatch, so just replace
							log.info(String.format("Unable to merge collection into array for '%s' (mismatched component type); replacing instead", entry.getKey()));
							return additionV;
						}
					}

					// Collection <- Array
					if (baseV.getClass().isArray() && additionV instanceof Collection<?>) {
						try {
							return (V) mergeArray(baseV.getClass().getComponentType(), (Object[]) baseV, (Collection) additionV);
						} catch (ClassCastException e) {
							// Type mismatch, so just replace
							log.info(String.format("Unable to merge array into collection for '%s' (mismatched component type); replacing instead", entry.getKey()));
							return additionV;
						}
					}

					// Map <- Map
					if (baseV instanceof Map && additionV instanceof Map) {
						mergeMaps((Map) baseV, (Map) additionV);
						return baseV;
					}

					if (baseV instanceof Substitutable && additionV instanceof Substitutable
							&& ((Substitutable) baseV).matches((Substitutable) additionV)) {
						((Substitutable) baseV).merge(((Substitutable) additionV));
						return baseV;
					}
					return additionV;
				});
			}
		}
	}

	/**
	 * Merge two collections together, preventing duplicates
	 *
	 * @throws ClassCastException	If any elements in the additions collection cannot be cast to the component type
	 */
	public static <T> void mergeCollection(Class<T> componentType, Collection<T> base, Collection<?> additions) {
		boolean isSubstitutable = Substitutable.class.isAssignableFrom(componentType);
		for (Object addObject : additions) {
			T addT = componentType.cast(addObject);

			boolean found = false;
			Iterator<?> baseIterator = base.iterator();
			while (baseIterator.hasNext() && !found) {
				T baseT = componentType.cast(baseIterator.next());
				if (isSubstitutable && ((Substitutable) baseT).matches((Substitutable) addT)) {
					((Substitutable) baseT).merge((Substitutable) addT);
					found = true;
				}
				else if (baseT.equals(addT)) {
					found = true;
				}
			}
			if (!found) {
				base.add(addT);
			}
		}
	}

	/**
	 * Merge an array into a collection, preventing duplicates
	 *
	 * @throws ClassCastException	If any elements in the additions collection cannot be cast to the component type
	 */
	public static <T> void mergeCollection(Class<T> componentType, Collection<T> base, Object[] additions) {
		mergeCollection(componentType, base, Arrays.asList(additions));
	}

	/**
	 * Merge two arrays together, preventing duplicates.
	 * Return a new array with the merged items.
	 *
	 * @throws ClassCastException	If any elements in the additions array cannot be cast to the component type
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] mergeArray(Class<T> componentType, Object[] base, Object[] additions) {
		List<T> merged = new ArrayList<>();
		mergeCollection(componentType, merged, Arrays.asList(base));
		mergeCollection(componentType, merged, Arrays.asList(additions));

		T[] mergedArray = (T[]) Array.newInstance(componentType, merged.size());
		int index = 0;
		for (Object obj : merged) {
			mergedArray[index] = componentType.cast(obj);
			index++;
		}

		return mergedArray;
	}

	/**
	 * Merge a collection into an array, preventing duplicates
	 * Return a new array with the merged items.
	 *
	 * @throws ClassCastException	If any elements in the additions array cannot be cast to the component type
	 */
	public static <T> T[] mergeArray(Class<T> componentType, Object[] base, Collection<?> additions) {
		return mergeArray(componentType, base, additions.toArray());
	}

	/**
	 * Validate the the specified object is acceptable as a substitution.
	 * An object is acceptable if it is {@link Serializable} and (if an array/collection/map), its contents
	 * are all {@code Serializable}
	 */
	public static void validateSubstitution(Object object) throws IllegalArgumentException {
		if (object == null) {
			return;
		}
		else if (object.getClass().isArray()) {
			if (!object.getClass().getComponentType().isPrimitive()
					&& !Serializable.class.isAssignableFrom(object.getClass().getComponentType())) {
			throw new IllegalArgumentException(String.format(
				"Non-serializable array type %s", object.getClass().getComponentType().getSimpleName()));
			}
		}
		else if (object instanceof Collection<?>) {
			for (Object element : ((Collection<?>) object)) {
				validateSubstitution(element);
			}
		}
		else if (object instanceof Map<?, ?>) {
			validateSubstitution(((Map<?, ?>) object).values());
		}
		else if (!(object instanceof Serializable)) {
			throw new IllegalArgumentException(String.format("Non-serializable object type %s", object.getClass().getSimpleName()));
		}
	}
}
