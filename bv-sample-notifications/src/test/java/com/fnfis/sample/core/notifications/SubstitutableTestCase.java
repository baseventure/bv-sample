package com.fnfis.sample.core.notifications;

import static com.fnfis.sample.core.notifications.SubstitutionUtils.mergeArray;
import static com.fnfis.sample.core.notifications.SubstitutionUtils.mergeCollection;
import static com.fnfis.sample.core.notifications.SubstitutionUtils.mergeMaps;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertTrue;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fnfis.sample.core.model.utils.DateUtils;

public class SubstitutableTestCase {
	@Test
	public void testInstance() {
		String baseName = "base";
		Date baseDate = DateUtils.startOf(YearMonth.now());
		TestSubstitutable base = createSubstitutable(1,	baseName, baseDate, "base", baseName);
		base.lookup.put("shared", baseName);

		String addName = "add";
		Date addDate = DateUtils.endOf(YearMonth.now());
		TestSubstitutable add = createSubstitutable(1, addName, addDate, "add", addName);
		add.lookup.put("shared", addName);

		assertTrue(base.matches(add));
		base.merge(add);

		assertThat(base.name, is(addName));
		assertThat(base.dates, hasSize(2));
		assertThat(base.lookup.get("base"), is(baseName));
		assertThat(base.lookup.get("add"), is(addName));
		assertThat(base.lookup.get("shared"), is(addName));
	}

	@Test
	public void testArray() {
		int sharedId = 10;

		String baseName = "base";
		Date baseDate = DateUtils.startOf(YearMonth.now());
		TestSubstitutable base = createSubstitutable(1,	baseName, baseDate, "base", baseName);
		TestSubstitutable baseShared = createSubstitutable(sharedId, baseName, baseDate, "base", baseName);
		TestSubstitutable[] baseSubstitutables = new TestSubstitutable[] { base, baseShared };

		String addName = "add";
		Date addDate = DateUtils.endOf(YearMonth.now());
		TestSubstitutable add = createSubstitutable(2, addName, addDate, "add", addName);
		TestSubstitutable addShared = createSubstitutable(sharedId, addName, addDate, "add", addName);
		TestSubstitutable[] addSubstitutables = new TestSubstitutable[] { add, addShared };

		TestSubstitutable[] mergeSubstitutables = mergeArray(TestSubstitutable.class, baseSubstitutables, addSubstitutables);

		assertThat(mergeSubstitutables, arrayWithSize(3));

		TestSubstitutable mergeShared = null;
		for (TestSubstitutable item : mergeSubstitutables) {
			if (item.id == sharedId) {
				mergeShared = item;
				break;
			}
		}
		assertThat(mergeShared, is(not(nullValue())));
		assertThat(mergeShared.name, is(addName));
		assertThat(mergeShared.dates, hasSize(2));
		assertThat(mergeShared.lookup.entrySet(), hasSize(2));
	}

	@Test
	public void testCollection() {
		int sharedId = 10;

		String baseName = "base";
		Date baseDate = DateUtils.startOf(YearMonth.now());
		TestSubstitutable base = createSubstitutable(1,	baseName, baseDate, "base", baseName);
		TestSubstitutable baseShared = createSubstitutable(sharedId, baseName, baseDate, "base", baseName);
		List<TestSubstitutable> baseSubstitutables = new ArrayList<>();
		baseSubstitutables.add(base);
		baseSubstitutables.add(baseShared);

		String addName = "add";
		Date addDate = DateUtils.endOf(YearMonth.now());
		TestSubstitutable add = createSubstitutable(2, addName, addDate, "add", addName);
		TestSubstitutable addShared = createSubstitutable(sharedId, addName, addDate, "add", addName);
		List<TestSubstitutable> addSubstitutables = new ArrayList<>();
		addSubstitutables.add(add);
		addSubstitutables.add(addShared);

		mergeCollection(TestSubstitutable.class, baseSubstitutables, addSubstitutables);

		assertThat(baseSubstitutables, hasSize(3));

		TestSubstitutable mergeShared = null;
		for (TestSubstitutable item : baseSubstitutables) {
			if (item.id == sharedId) {
				mergeShared = item;
				break;
			}
		}
		assertThat(mergeShared, is(not(nullValue())));
		assertThat(mergeShared.name, is(addName));
		assertThat(mergeShared.dates, hasSize(2));
		assertThat(mergeShared.lookup.entrySet(), hasSize(2));
	}

	@Test
	public void testMap() {
		int sharedId = 10;

		String baseName = "base";
		Date baseDate = DateUtils.startOf(YearMonth.now());
		TestSubstitutable base = createSubstitutable(1,	baseName, baseDate, "base", baseName);
		TestSubstitutable baseShared = createSubstitutable(sharedId, baseName, baseDate, "base", baseName);
		Map<String, TestSubstitutable> baseMap = new HashMap<>();
		baseMap.put("base", base);
		baseMap.put("shared", baseShared);

		String addName = "add";
		Date addDate = DateUtils.endOf(YearMonth.now());
		TestSubstitutable add = createSubstitutable(2, addName, addDate, "add", addName);
		TestSubstitutable addShared = createSubstitutable(sharedId, addName, addDate, "add", addName);
		Map<String, TestSubstitutable> addMap = new HashMap<>();
		addMap.put("add", add);
		addMap.put("shared", addShared);

		mergeMaps(baseMap, addMap);

		assertThat(baseMap.entrySet(), hasSize(3));

		TestSubstitutable mergeShared = baseMap.get("shared");
		assertThat(mergeShared, is(not(nullValue())));
		assertThat(mergeShared.name, is(addName));
		assertThat(mergeShared.dates, hasSize(2));
		assertThat(mergeShared.lookup.entrySet(), hasSize(2));
	}

	// UTILITY

	protected TestSubstitutable createSubstitutable(int id, String name, Date date, String key, String value) {
		TestSubstitutable instance = new TestSubstitutable(id);
		instance.name = name;
		instance.dates.add(date);
		instance.lookup.put(key, value);
		return instance;
	}
}
