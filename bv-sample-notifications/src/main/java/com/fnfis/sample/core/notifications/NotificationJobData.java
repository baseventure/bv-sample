package com.fnfis.sample.core.notifications;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javax.persistence.EntityManager;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fnfis.sample.core.schedule.Job;
import com.fnfis.sample.core.schedule.Timing;

/**
 */
public class NotificationJobData implements Externalizable {

	@JsonProperty("timing")
	private Timing timing;

	private NotificationData notificationData;

	// CONSTRUCTORS

	protected NotificationJobData() { super(); }
	public NotificationJobData(Job<NotificationData> job) {
		this();
		this.notificationData = job.getData();
		this.timing = job.getTiming();
	}

	// SERIALIZATION

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		notificationData.writeExternal(out);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		notificationData.readExternal(in);
	}

	@JsonProperty("notification-data")
	public NotificationData getNotificationData() {
		return notificationData;
	}

	// UTILITY

	/**
	 * Inflate the {@link TypedObjectRef}s (if any) using the specified entity manager.
	 */
	public NotificationJobData inflate(EntityManager entityManager) {
		notificationData.inflate(entityManager);
		return this;
	}
}
