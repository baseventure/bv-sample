package com.fnfis.sample.core.schedule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * A {@link JobQueue} implementation that uses and in-memory queue
 */
public class InMemoryJobQueue<T extends Serializable> implements JobQueue<T> {

	private List<Job<T>> jobs;

	// GETTER/SETTER

	protected List<Job<T>> basicGetJobs() {
		if (jobs == null) {
			jobs = new ArrayList<>();
		}
		return jobs;
	}

	// REQUIRED

	@Override
	public Collection<Job<T>> getJobs() {
		return new ArrayList<>(basicGetJobs());
	}

	@Override
	public Job<T> push(Job<T> job) {
		basicGetJobs().add(job);
		return job;
	}

	@Override
	public Collection<Job<T>> pushAllFor(Collection<T> data, Timing timing) {
		Collection<Job<T>> jobs = new ArrayList<>();
		for (T datum : data) {
			jobs.add(pushFor(datum, timing));
		}
		return jobs;
	}

	@Override
	public Job<T> pushFor(T datum, Timing timing) {
		return push(new Job<T>(datum, timing));
	}

	@Override
	public Collection<Job<T>> peekFor(T data) {
		List<Job<T>> matches = new LinkedList<>();
		for (Job<T> job : basicGetJobs()) {
			if (job.getData().equals(data)) {
				matches.add(job);
			}
		}
		return matches;
	}

	@Override
	public Collection<Job<T>> peekFor(Timing timing) {
		List<Job<T>> matches = new LinkedList<>();
		for (Job<T> job : basicGetJobs()) {
			if (job.getTiming().equals(timing)) {
				matches.add(job);
			}
		}
		return matches;
	}

	@Override
	public Collection<Job<T>> popFor(T data) {
		Collection<Job<T>> matches = peekFor(data);
		basicGetJobs().removeAll(matches);
		return matches;
	}

	@Override
	public Collection<Job<T>> popFor(Timing timing) {
		Collection<Job<T>> matches = peekFor(timing);
		basicGetJobs().removeAll(matches);
		return matches;
	}

	@Override
	public Collection<Job<T>> clear() {
		List<Job<T>> current = basicGetJobs();
		jobs = null;
		return current;
	}

}
