package com.fnfis.sample.core.notifications;

import org.simplejavamail.email.Email;

/**
 * Interface to support mail sending.
 * 
 * @author Tim Morrison
 */
/*
 * TCM
 * Technically, this is stupid/unnecessary, but simplemail makes the #sendMail method
 * final, which makes it impossible to override for testing purposes.  Hence this
 * local interface to wrapper the {@link Mailer} one.
 */
public interface MailSender {
	public boolean validate(final Email email);
	public void sendMail(final Email email);
}
