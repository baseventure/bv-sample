/* Expected binding:
 * Input
 * _subject: String
 * _content: String
 *
 * Output:
 * subject: String
 * content: GString
 */

 subject = "${SYSTEM_PROPERTY_VALUE}";
 htmlContent = "${_content}";
 content = "${_content}";
 