package com.fnfis.sample.core.model.utils.query;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnfis.sample.core.model.TypedObjectReference;
import com.fnfis.sample.core.model.BaseObject;
import com.fnfis.sample.core.model.TypedObjectReference;

/**
 * A set of queries that help handle the JPA Criteria functionality required.
 */
public class CriteriaQueries {
	private static final Logger log = LoggerFactory.getLogger(CriteriaQueries.class);

	/**
	 * Attempt to lookup the object specified by the {@link TypedObjectReference}
	 * and return that object.
	 */
	public static <M extends BaseObject> M findReferencedObject(
		EntityManager entityManager,
		TypedObjectReference reference)
	{
		M result = null;

		if (reference != null && reference.getTargetType() != null && reference.getTargetUuid() != null) {
			try {
				@SuppressWarnings("unchecked")
				Class<M> resultType =
					(Class<M>) Class.forName(reference.getTargetType());
				UUID targetUUID = UUID.fromString(reference.getTargetUuid());

				result = CriteriaQueries.findOneWithUUID(entityManager, resultType, targetUUID);
			} catch (ClassNotFoundException e) {
				log.warn("Error attempting to resolve typed object reference with type {}.  Returning null.", reference.getTargetType(), e);
			}
		}

		return result;
	}

	/**
	 * Attempt to find a single instance of the provided type with the UUID.
	 */
	public static <M extends BaseObject> M findOneWithUUID(EntityManager entityManager, Class<M> entityType, UUID uuid) {
		M result = null;

		try {
			//
			// (code removed for simplification)
			//
		} catch (NoResultException e) {
			// munch... go ahead and return null
		}

		return result;
	}
}
