package com.fnfis.sample.core.schedule;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

/**
 * An interface for a queue of {@link Job}s
 */
public interface JobQueue<T extends Serializable> extends Iterable<Job<T>> {

	/**
	 * Answer all of the jobs in the queue
	 */
	public Collection<Job<T>> getJobs();

	/**
	 * Add a {@link Job} to the queue.
	 */
	public Job<T> push(Job<T> job);

	/**
	 * Add a job to the queue for the specified datum, timing
	 */
	public Job<T> pushFor(T datum, Timing timing);

	/**
	 * Add {@link Job}s to the queue for the specified data, timing
	 */
	public Collection<Job<T>> pushAllFor(Collection<T> data, Timing timing);

	/**
	 * Find (but don't remove) all entries whose data is equivalent to the specified data
	 */
	public Collection<Job<T>> peekFor(T data);

	/**
	 * Find (but don't remove) all entries for the specified timing;
	 */
	public Collection<Job<T>> peekFor(Timing timing);

	/**
	 * Remove all entries whose data is equivalent to the specified data
	 */
	public Collection<Job<T>> popFor(T data);

	/**
	 * Remove all entries for the specified timing;
	 */
	public Collection<Job<T>> popFor(Timing timing);

	/**
	 * Clear the entries in the queue
	 */
	public Collection<Job<T>> clear();

	// REQUIRED -- Iterable

	@Override
	default Iterator<Job<T>> iterator() {
		return getJobs().iterator();
	}
}
