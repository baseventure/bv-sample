package com.fnfis.sample.core.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Base/root entity model type
 */
@MappedSuperclass
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="uuid")
public abstract class BaseObject implements Serializable {
	private static final long serialVersionUID = -3468622861798791790L;

	public enum ObjectStateEnum {
		NEW, SAVED, IN_PROGRESS, DELETED;
	}

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	protected Long id;

	@Type(type = "uuid-char")
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(length = 36, nullable = false, unique = true)
	protected UUID uuid;

	@Enumerated(javax.persistence.EnumType.STRING)
	@Column(nullable = false)
	@JsonProperty("object-state")
	protected ObjectStateEnum objectState;

	@Column(nullable = false)
	@JsonProperty("created-by")
	protected String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	@JsonProperty("created-on")
	protected Date createdOn;

	@Column(nullable = true)
	@JsonProperty("updated-by")
	protected String updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	@JsonProperty("updated-on")
	protected Date updatedOn;

	// CONSTRUCTORS

	public BaseObject() {
		super();
		objectState = ObjectStateEnum.NEW;
	}

	// GETTERS/SETTERS

	public long getId() { return this.id; }
	protected void setId(long myId) { this.id = myId; }
	public void unsetId() { this.id = 0L; }

	public UUID getUuid() { return this.uuid; }
	public void setUuid(UUID uuid) { this.uuid = uuid; }
	public void unsetUuid() { setUuid(null); }

	public ObjectStateEnum getObjectState() { return this.objectState; }
	public void setObjectState(ObjectStateEnum objectState) { this.objectState = objectState; }
	public void unsetObjectState() { setObjectState(ObjectStateEnum.NEW); }

	public String getCreatedBy() { return this.createdBy; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void unsetCreatedBy() { setCreatedBy(null); }

	public Date getCreatedOn() { return this.createdOn; }
	public void setCreatedOn(Date createdOn) { this.createdOn = createdOn; }
	public void unsetCreatedOn() { setCreatedOn(null); }

	public String getUpdatedBy() { return this.updatedBy; }
	public void setUpdatedBy(String updatedBy) { this.updatedBy = updatedBy; }
	public void unsetUpdatedBy() { setUpdatedBy(null); }

	public Date getUpdatedOn() { return this.updatedOn; }
	public void setUpdatedOn(Date updatedOn) { this.updatedOn = updatedOn; }
	public void unsetUpdatedOn() {setUpdatedOn(null); }

	// DERIVED

	/**
	 * Return a boolean indicating whether this object has been associated with
	 * an identifier.
	 */
	public boolean hasId() {
		return (this.id != null) && (this.id.longValue() != 0L);
	}

	/**
	 * Answer a displayable name for the receiver
	 */
	@JsonIgnore
	public String getDisplayName() {
		return (getUuid() == null) ? "(unknown)" : getUuid().toString();
	}

	/**
	 * Return a boolean indicating whether this object has never been saved.
	 */
	@JsonIgnore
	public boolean isUnsavedObject() {
		return
			(objectState == ObjectStateEnum.NEW) ||
			(id == null) ||
			(uuid == null) ||
			(createdBy == null) ||
			(createdOn == null);
	}

	/**
	 * Answer a boolean indicating if the receiver's object state is deleted
	 */
	@JsonIgnore
	public boolean isDeletedObject() {
		return objectState == ObjectStateEnum.DELETED;
	}

	// PERSISTENCE

	@PrePersist
	public void prePersist() {
		if (objectState != ObjectStateEnum.DELETED) {
			objectState = ObjectStateEnum.SAVED;
		}

		if (uuid == null) {
			uuid = UUID.randomUUID();
		}

		if (createdBy == null) {
			createdBy = "SYSTEM";
		}

		if (createdOn == null) {
			createdOn = new Date();
		}
	}

	@PreUpdate
	public void preUpdate() {
		if (updatedBy == null) {
			updatedBy = "SYSTEM";
		}

		if (updatedOn == null) {
			updatedOn = new Date();
		}
	}

	/**
	 * Reload this object from the database getting an up to date copy.
	 */
	@SuppressWarnings("unchecked")
	public <T extends BaseObject> T reload(EntityManager entityManager) {
		return (T) entityManager.find(this.getClass(), this.getId());
	}

	// UTILITY

	/**
	 * Mark the receiver's object state as having been deleted.
	 */
	public void markDeleted() {
		setObjectState(ObjectStateEnum.DELETED);
	}

	/**
	 * Return a boolean indicating whether this object appears to be
	 * detached from the specified {@link EntityManager}.  NOTE:
	 * This is not foolproof.
	 *
	 * @param entityManager
	 * @return
	 */
	@JsonIgnore
	public boolean isDetached(EntityManager entityManager) {
		return (id != null) && !entityManager.contains(this);
	}

	// OVERRIDES

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseObject other = (BaseObject) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		sb.append("[").append(uuid).append("]");

		return sb.toString();
	}

	// UTILITY

	/**
	 * Return a single result from the list or <code>null</code> if the list is empty.
	 */
	protected static <T> T getSingleResult(List<T> results) {
		return results.isEmpty() ? null : results.get(0);
	}

	/**
	 * Wrap the specified {@link BaseObject}'s in a new {@link Set}.
	 * If the specified object is <code>null</code> this method
	 * will return an empty set.
	 */
	protected Set<BaseObject> wrapObjectsInSet(BaseObject ...objects) {
		Set<BaseObject> result = new HashSet<>(objects.length);

		for (BaseObject object : objects) {
			if (object != null) {
				result.add(object);
			}
		}

		return result;
	}
}
