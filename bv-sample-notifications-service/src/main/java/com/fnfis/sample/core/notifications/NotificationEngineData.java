package com.fnfis.sample.core.notifications;

import javax.persistence.EntityManager;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data type representing information about the {@link NotificationEngine}
 */
public class NotificationEngineData {
	@JsonProperty("smtp-host")
	private String smtpHost;

	@JsonProperty("smtp-port")
	private Integer smtpPort;

	@JsonProperty("smtp-user")
	private String smtpUser;

	@JsonProperty("email-domain")
	private String emailDomain;

	@JsonProperty("active-queue-data")
	private NotificationQueueData activeQueueData;

	@JsonProperty("dead-queue-data")
	private NotificationQueueData deadQueueData;

	// CONSTRUCTORS

	public NotificationEngineData() { super(); }
	public NotificationEngineData(NotificationEngine engine) {
		this();
		setSmtpHost(engine.smtpHost);
		setSmtpPort(engine.smtpPort);
		setSmtpUser(engine.smtpUsername);
		setEmailDomain(engine.emailDomain);
		setActiveQueueData(new NotificationQueueData(engine.getActiveQueue()));
		setDeadQueueData(new NotificationQueueData(engine.getDeadQueue()));
	}

	// GETTERS/SETTERS

	public String getSmtpHost() { return smtpHost; }
	public void setSmtpHost(String smtpHost) { this.smtpHost = smtpHost; }
	public Integer getSmtpPort() { return smtpPort; }
	public void setSmtpPort(Integer smtpPort) { this.smtpPort = smtpPort; }
	public String getSmtpUser() { return smtpUser; }
	public void setSmtpUser(String smptUser) { this.smtpUser = smptUser; }
	public String getEmailDomain() { return emailDomain; }
	public void setEmailDomain(String emailDomain) { this.emailDomain = emailDomain; }
	public NotificationQueueData getActiveQueueData() { return activeQueueData; }
	public void setActiveQueueData(NotificationQueueData queueData) { this.activeQueueData = queueData; }
	public NotificationQueueData getDeadQueueData() { return deadQueueData; }
	public void setDeadQueueData(NotificationQueueData queueData) { this.deadQueueData = queueData; }

	// INFLATION

	/**
	 * Inflate the information in the data
	 */
	public NotificationEngineData inflate(EntityManager entityManager) {
		if (entityManager != null) {
			if (getActiveQueueData() != null) {
				getActiveQueueData().inflate(entityManager);
			}
			if (getDeadQueueData() != null) {
				getDeadQueueData().inflate(entityManager);
			}
		}
		return this;
	}
}
