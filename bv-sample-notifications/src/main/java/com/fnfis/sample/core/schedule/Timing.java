package com.fnfis.sample.core.schedule;

/**
 * Enumeration of possible timing for queuing of jobs
 */
public enum Timing {
	// NOTE: The ordering of these values is important, as they imply an order in terms of speed
	// of delivery. For instance, when retrying the sending of email, the system will bump the timing
	// to the next fastest timing (eg. Hour -> Immediate) on error.  Make sure that
	// any changes to this enumeration properly account for the speed of delivery in the ordering.
	IMMEDIATE,
	HOUR,
	DAY,
	NONE;		//Used in client-server communications to indicate that no notification is to be sent.

	/**
	 * Answer an slower timing based on a current timing (up to the slowest)
	 */
	public Timing slower() {
		Timing[] vals = values();
		return vals[Math.min(this.ordinal() + 1, vals.length - 1)];
	}

	/**
	 * Answer a faster timing based on a current timing (up to the fastest)
	 */
	public Timing faster() {
		Timing[] vals = values();
		return vals[Math.max(this.ordinal() - 1, 0)];
	}

}
