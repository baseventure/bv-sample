package com.fnfis.sample.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fnfis.sample.core.model.utils.query.CriteriaQueries;

/**
 * Abstract superclass of the typed object reference implementations.
 * Puts the common functionality in a shared place to allow for
 * separate {@link Entity} and {@link Embeddable} implementations.
 */
public class TypedObjectReference implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	protected Long id;

	@Column
	protected String targetType;
	@Column
	protected String targetUuid;

	// CONSTRUCTORS

	public TypedObjectReference() {}
	public TypedObjectReference(String type, String uuid) {
		super();
		this.targetType = type;
		this.targetUuid = uuid;
	}

	/**
	 * Construct a reference to the specified target object.
	 */
	public TypedObjectReference(BaseObject target) {
		if (target != null) {
			this.targetType = target.getClass().getName();

			if (target.getUuid() != null) {
				this.targetUuid = target.getUuid().toString();
			}
		}
	}

	// GETTERS/SETTERS

	public String getTargetType() { return targetType; }
	public void setTargetType(String type) { this.targetType = type; }

	public String getTargetUuid() { return targetUuid; }
	public void setTargetUuid(String uuid) { this.targetUuid = uuid; }

	// UTILITY

	/**
	 * Attempt to retrieve an instance of the object referenced
	 * by this object reference using the specified {@link EntityManager}.
	 */
	public <M extends BaseObject> M retrieve(EntityManager entityManager) {
		return CriteriaQueries.findReferencedObject(entityManager, this);
	}


	// OVERRIDES

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((targetType == null) ? 0 : targetType.hashCode());
		result = prime * result + ((targetUuid == null) ? 0 : targetUuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypedObjectReference other = (TypedObjectReference) obj;
		if (targetType == null) {
			if (other.targetType != null)
				return false;
		} else if (!targetType.equals(other.targetType))
			return false;
		if (targetUuid == null) {
			if (other.targetUuid != null)
				return false;
		} else if (!targetUuid.equals(other.targetUuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("TypedObjectReference [targetType=");
		sb.append(targetType).append(", targetUuid=").append(targetUuid).append("]");

		return sb.toString();
	}
}
