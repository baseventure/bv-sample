package com.fnfis.sample.core.notifications;

import static com.fnfis.sample.core.notifications.SubstitutionUtils.mergeCollection;
import static com.fnfis.sample.core.notifications.SubstitutionUtils.mergeMaps;
import static com.fnfis.sample.core.notifications.SubstitutionUtils.typeMatches;
import static com.fnfis.sample.core.notifications.SubstitutionUtils.validateSubstitution;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import groovy.lang.Binding;
import groovy.lang.GroovyRuntimeException;
import groovy.lang.GroovyShell;

/**
 * The content information for a notification
 */
public class Notification implements Substitutable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(Notification.class);

	// INTERNAL TYPES

	/**
	 * (Internal) representation of an embedded resource
	 */
	public class Resource implements Serializable {
		private static final long serialVersionUID = 1L;

		private String name;
		private byte[] data;
		private String type;

		public Resource(String name, byte[] data, String type) {
			this.name = name;
			this.data = data;
			this.type = type;
		}

		public String getName() { return name; }
		public byte[] getData() { return data; }
		public String getType() { return type; }

		@Override
		public int hashCode() {
			return Objects.hashCode(name) + Objects.hashCode(type);
		}

		@Override
		public boolean equals(final Object obj) {
			if(this == obj) return true;
			if((obj == null) || (obj.getClass() != this.getClass())) return false;
			Resource resource = (Resource) obj;
			return Objects.equals(name, resource.getName()) && Objects.equals(type, resource.getType());
		}

		@Override
		public String toString() {
			return "Resource{" +
					"\n\t\tname='" + name + '\'' +
					",\n\t\ttype=" + type +
					"\n\t}";
		}
	}

	// PROPERTIES

	@JsonProperty("template-name")
	private String templateName;

	@JsonIgnore
	private Map<String, Serializable> substitutions;
	@JsonIgnore
	private List<Resource> embeddedImages;
	@JsonIgnore
	private List<Resource> attachments;

	// CONSTRUCTORS

	public Notification(String templateName) {
		super();
		setTemplateName(templateName);
	}

	// GETTERS/SETTERS

	public String getTemplateName() { return templateName; }
	public void setTemplateName(String templateName) { this.templateName = templateName; }

	public Map<String, Serializable> getSubstitutions() {
		if (substitutions == null) {
			substitutions = new HashMap<>();
		}
		return substitutions;
	}
	public Serializable getSubstitution(String key) { return getSubstitutions().get(key); }

	public void addSubstitution(String key, Serializable value) throws IllegalArgumentException {
		validateSubstitution(value);
		getSubstitutions().put(key,  value);
	}
	public void addSubstitutions(Map<String, Serializable> substitutions) throws IllegalArgumentException {
		validateSubstitution(substitutions);
		getSubstitutions().putAll(substitutions);
	}

	public List<Resource> getEmbeddedImages() {
		if (embeddedImages == null) {
			embeddedImages = new ArrayList<>();
		}
		return embeddedImages;
	}
	public void addEmbeddedImage(String name, byte[] data, String mimetype) {
		getEmbeddedImages().add(new Resource(name, data, mimetype));
	}
	protected void addEmbeddedImages(List<Resource> embeddedImages) {
		getEmbeddedImages().addAll(embeddedImages);
	}

	public List<Resource> getAttachments() {
		if (attachments == null) {
			attachments = new ArrayList<>();
		}
		return attachments;
	}
	public void addAttachment(String name, byte[] data, String mimetype) {
		getAttachments().add(new Resource(name, data, mimetype));
	}
	protected void addAttachments(List<Resource> attachments) {
		getAttachments().addAll(attachments);
	}

	// DERIVED

	/**
	 * Apply the notifications template to produce a {@link Map} of results.
	 * In general, the result will contain information for generating an {@link Email}
	 * (e.g. subject, html content, etc.)
	 */
	public Map<?, ?> applyTemplate() throws NotificationException {
		return applyTemplate(null);
	}

	/**
	 * Apply the notifications template to produce a {@link Map} of results, given
	 * the set of additional bindings. In general, the result will contain information for
	 * generating an {@link Email} (e.g. subject, html content, etc.)
	 */
	public Map<?, ?> applyTemplate(Map<String, Object> additions) throws NotificationException {
		// TCM: a bit of a hack, but it let's us generate retryable exception for testing purposes
		if (isRetryTemplate()) {
			NotificationException retryException = new NotificationException("Retryable template for testing purposes");
			retryException.setRetryable(true);
			throw retryException;
		}

		Map<String, Object> subs = new HashMap<>(getSubstitutions());
		if (additions != null) {
			mergeMaps(subs, additions);
		}
		Binding binding = new Binding(subs);
		GroovyShell shell = new GroovyShell(binding);
		if (getTemplateName() != null) {
			try {
				InputStream templateStream = this.getClass().getResourceAsStream(getTemplateName());
				if (templateStream == null) {
					throw new NotificationException(String.format("Error applying template; no template named %s found", getTemplateName()));
				}
				shell.evaluate(new InputStreamReader(templateStream));
			} catch (GroovyRuntimeException e) {
				log.warn("Error applying template", e.getMessage());
				throw new NotificationException("Error applying template", e);
			}
		}
		return binding.getVariables();
	}

	// SUBSTITUION

	@Override
	public boolean matches(Substitutable substitution) {
		return typeMatches(this, substitution)
			&& Objects.equals(templateName, ((Notification) substitution).getTemplateName());
	}

	@Override
	public void merge(Substitutable substitution) {
		if (!matches(substitution)) {
			throw new IllegalArgumentException("Non matching notification");
		}

		Notification notification = (Notification) substitution;
		mergeMaps(getSubstitutions(), notification.getSubstitutions());
		mergeCollection(Resource.class, getEmbeddedImages(), notification.getEmbeddedImages());
		mergeCollection(Resource.class, getAttachments(), notification.getAttachments());
	}

	// TESTING

	private static final String TESTING_TEMPLATE_RETRY = "__retry.groovy";

	public boolean isRetryTemplate() {
		return TESTING_TEMPLATE_RETRY.equals(getTemplateName());
	}
	public void useRetryTemplate() {
		setTemplateName(TESTING_TEMPLATE_RETRY);
	}
}
