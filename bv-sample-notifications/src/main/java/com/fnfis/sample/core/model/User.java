package com.fnfis.sample.core.model;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entity representing a user of the application
 */
@Entity
@Cacheable(true)
@NamedQueries({
	@NamedQuery(name=User.FIND_USER_BY_LOGIN_QUERY_NAME, query=User.FIND_USER_BY_LOGIN_QUERY),
	@NamedQuery(name=User.FIND_ALL_USERS_QUERY_NAME, query=User.FIND_ALL_USERS_QUERY),
	@NamedQuery(name=User.FIND_RECENT_LOGIN_ATTEMPTS_QUERY_NAME, query=User.FIND_RECENT_LOGIN_ATTEMPTS)
})
public class User extends BaseObject {
	private static final long serialVersionUID = 5102145052476312387L;
	private static final String FALLBACK_NAME = "_";

	public static final String FIND_ALL_USERS_QUERY_NAME = "FIND_ALL_USERS_QUERY";
	protected static final String FIND_ALL_USERS_QUERY = "SELECT u FROM User AS u";

	public static final String FIND_USER_BY_LOGIN_QUERY_NAME = "FIND_USER_BY_LOGIN_QUERY";
	protected static final String FIND_USER_BY_LOGIN_QUERY = "SELECT u FROM User as u WHERE LOWER(u.login) = LOWER(:login)";

	public static final String FIND_RECENT_LOGIN_ATTEMPTS_QUERY_NAME = "FIND_RECENT_LOGIN_ATTEMPTS";
	protected static final String FIND_RECENT_LOGIN_ATTEMPTS = "SELECT u.lastLoginAttempt FROM User u ORDER BY u.lastLoginAttempt DESC";

	@Column(nullable = false, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty("first-name")
	protected String firstName;

	@Column(nullable = false, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty("last-name")
	protected String lastName;

	@Column(nullable = false, unique = true, length=190, columnDefinition = "varchar(190) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	protected String login;

	@Column(nullable = false)
	protected boolean activated;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	@JsonProperty("activated-on")
	protected Date activatedOn;

	@JsonIgnore
	@Column(nullable = true)
	protected String ldapDN;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	@JsonProperty("last-login-attempt")
	protected Date lastLoginAttempt;

	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(nullable = true)
	@JsonProperty("last-successful-login")
	protected Date lastSuccessfulLogin;

	// CONSTRUCTORS

	public User() { super(); }

	// GETTERS/SETTERS

	public String getFirstName() { return this.firstName; }
	public void setFirstName(String myFirstName) { this.firstName = myFirstName; }
	public String getLastName() { return this.lastName; }
	public void setLastName(String myLastName) { this.lastName = myLastName; }
	public String getLogin() { return (this.login == null) ? null : this.login.trim(); }
	public void setLogin(String myLogin) { this.login = myLogin; }
	public boolean getActivated() { return this.activated; }
	public void setActivated(boolean myActivated) {
		this.activated = myActivated;
		if (myActivated) {
			this.setActivatedOn(new Date());
		} else {
			this.setActivatedOn(null);
		}
	}
	public Date getActivatedOn() { return this.activatedOn; }
	public void setActivatedOn(Date activatedOn) { this.activatedOn = activatedOn; }
	public String getLdapDN() { return this.ldapDN; }
	public void setLdapDN(String myLdapDN) { this.ldapDN = myLdapDN; }
	public Date getLastLoginAttempt() { return this.lastLoginAttempt; }
	public void setLastLoginAttempt(Date myLastLoginAttempt) { this.lastLoginAttempt = myLastLoginAttempt; }
	public Date getLastSuccessfulLogin() { return this.lastSuccessfulLogin; }
	public void setLastSuccessfulLogin(Date myLastSuccessfulLogin) { this.lastSuccessfulLogin = myLastSuccessfulLogin; }

	// DERIVED

	@JsonIgnore
	public String getFullName() {
		StringBuilder builder = new StringBuilder();
		if (getFirstName() != null) {
			padAppend(builder, getFirstName(), ' ');
		}
		if (getLastName() != null) {
			padAppend(builder, getLastName(), ' ');
		}
		return builder.toString();
	}

	// OVERRIDES

	@Override
	public void prePersist() {
		super.prePersist();
		forceNonEmptyNameFields();

		// BPD-2887 - Force logins to lowercase
		if (this.login != null) {
			this.login = this.login.toLowerCase();
		}
	}

	@Override
	public void preUpdate() {
		super.preUpdate();
		forceNonEmptyNameFields();

		// BPD-2887 - Force logins to lowercase
		if (this.login != null) {
			this.login = this.login.toLowerCase();
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		sb.append("[").append(login).append("]");

		return sb.toString();
	}

	@Override
	@JsonIgnore
	public String getDisplayName() {
		return getLogin();
	}

	// UTILITY

	/**
	 * Append the given value into the given builder, padding with the given pad if
	 * the builder is non-empty
	 */
	public static void padAppend(StringBuilder builder, String value, char pad) {
		if (builder.length() > 0) {
			builder.append(pad);
		}
		builder.append(value);
	}

	/**
	 * To avoid issues downstream with LDAP and activation, make
	 * sure there are non-null and non-empty string values for both
	 * first and last name.
	 */
	private void forceNonEmptyNameFields() {
		if ((firstName == null) || firstName.trim().isEmpty()) {
			firstName = FALLBACK_NAME;
		}

		if ((lastName == null) || lastName.trim().isEmpty()) {
			lastName = FALLBACK_NAME;
		}
	}
}
