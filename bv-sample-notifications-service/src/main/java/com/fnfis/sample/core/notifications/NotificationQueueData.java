package com.fnfis.sample.core.notifications;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import com.fnfis.sample.core.schedule.JobQueue;

/**
 * A data type representing information about a {@link JobQueue}
 */
public class NotificationQueueData {
	private List<NotificationJobData> notificationJobs;

	// CONSTRUCTORS
	public NotificationQueueData() { super(); }
	public NotificationQueueData(JobQueue<NotificationData> queue) {
		this();
		if (queue != null) {
			setNotifications(queue.getJobs().stream()
				.map(job -> new NotificationJobData(job))
				.collect(Collectors.toList()));
		}
	}

	// GETTERS/SETTERS

	public List<NotificationJobData> getNotificationJobs() {
		if (notificationJobs == null) {
			notificationJobs = new ArrayList<>();
		}
		return notificationJobs;
	}
	protected void setNotifications(List<NotificationJobData> notifications) { this.notificationJobs = notifications; }

	// INFLATION

	/**
	 * Inflate the information in the data
	 */
	public NotificationQueueData inflate(EntityManager entityManager) {
		if (entityManager != null) {
			for (NotificationJobData notification : getNotificationJobs()) {
				notification.inflate(entityManager);
			}
		}
		return this;
	}
}
