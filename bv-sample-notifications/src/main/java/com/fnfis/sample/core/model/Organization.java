package com.fnfis.sample.core.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * An entity representing a business organization
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Cacheable(true)
public class Organization extends BaseObject implements Contactable {
	private static final long serialVersionUID = 8003026911488037635L;

	@Column(nullable = false, unique = true, length=128)
	protected String code;

	@Column(nullable = false, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	protected String name;

	@OneToMany
	@JoinColumn(name="org_id")
	@OrderColumn
	protected List<Contact> contacts;

	// CONSTRUCTORS

	public Organization() { super(); }

	// GETTERS/SETTERS

	// Code
	public String getCode() { return this.code; }
	public void setCode(String myCode) { this.code = myCode; }
	public void unsetCode() { this.code = ""; }

	// Name
	public String getName() { return this.name; }
	public void setName(String myName) { this.name = myName; }
	public void unsetName() { this.name = ""; }

	// Contact
	@Override
	public List<Contact> getContacts() {
		if (this.contacts == null) {
			this.contacts = new ArrayList<Contact>();
		}
		return this.contacts;
	}
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	// OVERRIDES

	@JsonIgnore
	@Override
	public Contact getPrimaryContact() { return Contactable.super.getPrimaryContact(); }

	@Override
	@JsonIgnore
	public String getDisplayName() { return getName(); }
}
