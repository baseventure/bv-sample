package com.fnfis.sample.core.common.lifecycle;

/**
 * Instances of this interface will receive a callback over the lifecycle of the application
 */
public interface LifecycleListener {
	/**
	 * Called when the application is starting up.
	 */
	public void onStartup();

	/**
	 * Called when the application is shutting down.
	 */
	public void onShutdown();
}
