/* Expected binding:
 * Input
 * _subject: String
 * _content: String
 *
 * Output:
 * subject: String
 * content: GString
 */

 subject = _subject;
 htmlContent = "${_content}";
 content = "${_content}";
 