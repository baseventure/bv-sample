package com.fnfis.sample.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entity representation of a individual contact
 */
@Entity
public class Contact extends BaseObject
{
	private static final long serialVersionUID = -538200793242038745L;

	@Column(nullable = true, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty
	protected String title;

	@Column(nullable = true, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty("first-name")
	protected String firstName;

	@Column(nullable = true, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty("middle-name")
	protected String middleName;

	@Column(nullable = true, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty("last-name")
	protected String lastName;

	@Column(nullable = true, columnDefinition = "varchar(255) collate utf8mb4_bin")		// BPD-1700: Case-sensitive collation (MySQL Specific)
	@JsonProperty
	protected String suffix;

	protected String primaryEmail;

	// CONSTRUCTORS

	public Contact() { super(); }

	public Contact(String title, String firstName, String middleName, String lastName, String suffix) {
		this();
		setTitle(title);
		setFirstName(firstName);
		setMiddleName(middleName);
		setLastName(lastName);
		setSuffix(suffix);
	}

	// GETTERS/SETTERS

	public String getTitle() { return title; }
	public void setTitle(String title) { this.title = title; }

	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }

	public String getMiddleName() { return middleName; }
	public void setMiddleName(String middleName) { this.middleName = middleName; }

	public String getLastName() { return lastName; }
	public void setLastName(String lastName) { this.lastName = lastName; }

	public String getSuffix() { return suffix; }
	public void setSuffix(String suffix) { this.suffix = suffix; }

	public String getPrimaryEmail() { return primaryEmail; }
	public void setPrimaryEmail(String primaryEmail) { this.primaryEmail = primaryEmail; }

	@JsonIgnore
	public String getFullName() {
		StringBuilder builder = new StringBuilder();

		if (!StringUtils.isEmpty(getTitle())) {
			padAppend(builder, getTitle(), ' ');
		}
		if (!StringUtils.isEmpty(getFirstName())) {
			padAppend(builder, getFirstName(), ' ');
		}
		if (!StringUtils.isEmpty(getMiddleName())) {
			padAppend(builder, getMiddleName(), ' ');
		}
		if (!StringUtils.isEmpty(getLastName())) {
			padAppend(builder, getLastName(), ' ');
		}
		if (!StringUtils.isEmpty(getSuffix())) {
			padAppend(builder, getSuffix(), ' ');
		}

		return builder.toString();
	}

	@Override
	@JsonIgnore
	public String getDisplayName() {
		StringBuilder builder = new StringBuilder();

		if (!(StringUtils.isEmpty(getLastName()) && StringUtils.isEmpty(getSuffix()))) {
			if (!StringUtils.isEmpty(getLastName())) {
				padAppend(builder, getLastName(), ' ');
			}
			if (!StringUtils.isEmpty(getSuffix())) {
				padAppend(builder, getSuffix(), ' ');
			}
			builder.append(',');
		}
		if (!StringUtils.isEmpty(getTitle())) {
			padAppend(builder, getTitle(), ' ');
		}
		if (!StringUtils.isEmpty(getFirstName())) {
			padAppend(builder, getFirstName(), ' ');
		}
		if (!StringUtils.isEmpty(getMiddleName())) {
			padAppend(builder, getMiddleName(), ' ');
		}

		return builder.toString();
	}

	// UTILITY

	/**
	 * Answer whether the receiver is considered empty of property values
	 */
	public boolean isEmpty() {
		return
			StringUtils.isEmpty(getTitle()) &&
			StringUtils.isEmpty(getFirstName()) &&
			StringUtils.isEmpty(getMiddleName()) &&
			StringUtils.isEmpty(getLastName()) &&
			StringUtils.isEmpty(getSuffix()) &&
			StringUtils.isEmpty(getPrimaryEmail());
	}

	/**
	 * Answer whether the receiver is considered valid. A contact is valid if
	 * it has a last name
	 */
	public boolean isValid() {
		return !StringUtils.isEmpty(getLastName());
	}

	/**
	 * Copy the information from the specified contact into the receiver.
	 */
	public void copyFrom(Contact contact) {
		if (contact != null) {
			setTitle(contact.getTitle());
			setFirstName(contact.getFirstName());
			setMiddleName(contact.getMiddleName());
			setLastName(contact.getLastName());
			setSuffix(contact.getSuffix());
			setPrimaryEmail(contact.getPrimaryEmail());
		}
	}

	// OVERRIDES

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getClass().getSimpleName());
		sb.append("[").append(getFullName()).append("]");
		return sb.toString();
	}

	// UTILITY

	/**
	 * Append the given value into the given builder, padding with the given pad if
	 * the builder is non-empty
	 */
	public static void padAppend(StringBuilder builder, String value, char pad) {
		if (builder.length() > 0) {
			builder.append(pad);
		}
		builder.append(value);
	}
}
