package com.fnfis.sample.core.model;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Interface for entities that are contactable (i.e. have {@link Contact}s)
 */
public interface Contactable {
	/**
	 * Answer the contacts for the receiver
	 */
	public List<Contact> getContacts();

	/**
	 * Add multiple contact to the receiver
	 */
	default void addAllContacts(Set<Contact> newContacts) { getContacts().addAll(newContacts); }

	/**
	 * Add a contact to the receiver
	 */
	default void addContacts(Contact newContact) { getContacts().add(newContact); }

	/**
	 * Remove all of the specified contacts from the receiver
	 */
	default void removeAllContacts(List<Contact> newContacts) { getContacts().removeAll(newContacts); }

	/**
	 * Remove the specified contact from the receiver
	 */
	default void removeContacts(Contact oldContacts) { getContacts().remove(oldContacts); }

	/**
	 * Answer the primary contact for the receiver.  The primary contact is assumed to be
	 * the first contact
	 */
	@JsonIgnore
	default Contact getPrimaryContact() { return getContacts().isEmpty() ? null : getContacts().get(0); }

	/**
	 * Set the primary contact for the receiver.  The primary contact is assumed to be
	 * the first contact
	 */
	default void setPrimaryContact(Contact contact) {
		if (contact != null) {
			getContacts().remove(contact);
			getContacts().add(0, contact);
		}
	}
}
