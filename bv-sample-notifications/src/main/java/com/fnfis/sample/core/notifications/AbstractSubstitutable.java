package com.fnfis.sample.core.notifications;

public abstract class AbstractSubstitutable implements Substitutable {
	private static final long serialVersionUID = 1L;

	@Override
	public boolean matches(Substitutable substitution) {
		// By default a substitution matches if it is equivalent
		return equals(substitution);
	}
}
