package com.fnfis.sample.core.notifications;

import java.io.Serializable;

/**
 * Interface to represent objects that can be explicitly used as substitutions in {@link Notification}s.
 * Substitutions support participating in the merge process associated with collapsing related
 * notifications into a single notification.
 */
public interface Substitutable extends Serializable {

	/**
	 * Answer a boolean indicating whether the receiver matches the specified substitution.
	 * A match indicates that the substitution can be merged into the receiver.
	 */
	public boolean matches(Substitutable substitution);

	/**
	 * Merge the specified substitution into the receiver (destructively).
	 * Note that merge will only be called on an object that {@link #matches(Substitutable)}
	 * the receiver.
	 */
	public void merge(Substitutable substitution);
}
