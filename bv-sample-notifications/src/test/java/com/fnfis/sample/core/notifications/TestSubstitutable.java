package com.fnfis.sample.core.notifications;

import static com.fnfis.sample.core.notifications.SubstitutionUtils.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fnfis.sample.core.notifications.AbstractSubstitutable;
import com.fnfis.sample.core.notifications.Substitutable;

public class TestSubstitutable extends AbstractSubstitutable {

	private static final long serialVersionUID = 1L;
	
	public int id;
	
	public String name;
	public Collection<Date> dates;
	public Map<String, String> lookup;
	
	// CONSTRUCTORS
	
	public TestSubstitutable(int id) {
		super();
		this.id = id;
		this.dates = new ArrayList<>();
		this.lookup = new HashMap<>();
	}
	
	// REQUIRED - Substitutable
	
	@Override
	public void merge(Substitutable substitution) {
		if (!matches(substitution)) throw new IllegalArgumentException("Non-matching TestObject");
		TestSubstitutable source = (TestSubstitutable) substitution;
		this.name = source.name;
		mergeCollection(Date.class, this.dates, source.dates);
		mergeMaps(this.lookup, source.lookup);
	}
	
	// OVERRIDE
	
	@Override
	public boolean equals(Object obj) {
		return typeMatches(this, obj) && this.id == ((TestSubstitutable) obj).id;
	}
	
	@Override
	public int hashCode() { return id; }
}
