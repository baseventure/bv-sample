package com.fnfis.sample.core.notifications;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.commons.lang3.StringUtils;
import org.hazlewood.connor.bottema.emailaddress.EmailAddressValidator;
import org.simplejavamail.MailException;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.Recipient;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.config.ServerConfig;
import org.simplejavamail.mailer.config.TransportStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnfis.sample.core.common.lifecycle.LifecycleListener;
import com.fnfis.sample.core.model.BaseObject;
import com.fnfis.sample.core.model.Organization;
import com.fnfis.sample.core.model.User;
import com.fnfis.sample.core.schedule.InMemoryJobQueue;
import com.fnfis.sample.core.schedule.Job;
import com.fnfis.sample.core.schedule.JobQueue;
import com.fnfis.sample.core.schedule.Timing;

/**
 * (Singleton) engine for queueing/sending {@link Notification}s.
 */
@Singleton
public class NotificationEngine implements LifecycleListener {
	private static final Logger log = LoggerFactory.getLogger(NotificationEngine.class);

	// CONSTANTS

	public static final String ACTIVE_QUEUE_NAME = "active";
	public static final String DEAD_QUEUE_NAME = "dead";

	// DEFAULTS

	protected static final String DEFAULT_EMAIL_NAME = "notifications";
	protected static final int DEFAULT_EMAIL_REQUEUE_LIMIT = 2;
	protected static final int DEFAULT_EMAIL_RATE_LIMIT = 1;		// Per Second

	protected static final String DEFAULT_SUBJECT = "Notification from fundmanager.io";

	@Inject		// (from external configuration)
	protected String smtpHost;

	@Inject		// (from external configuration)
	protected Integer smtpPort;

	@Inject		// (from external configuration)
	protected String smtpUsername;

	@Inject		// (from external configuration)
	protected String smtpPassword;

	@Inject		// (from external configuration)
	protected String emailName;

	@Inject		// (from external configuration)
	protected String emailDomain;

	@Inject		// (from external configuration)
	protected Integer emailRequeueLimit;

	@Inject		// (from external configuration)
	protected Integer emailRateLimit;

	@Inject		// (from external configuration)
	protected boolean emailDebug;

	/**
	 * Answer a boolean indicating if the SMTP configuration is valid
	 */
	protected boolean hasValidSmtpConfig() {
		return smtpHost != null
			&& smtpPort > 0
			&& smtpUsername != null
			&& smtpPassword != null;
	}

	// INTERNAL PROPERTIES

	private MailSender mailSender;
	private JobQueue<NotificationData> activeQueue;
	private JobQueue<NotificationData> deadQueue;

	@PersistenceContext(unitName = "bv-core-pu")
	private EntityManager entityManager;

	// GETTER/SETTER

	/**
	 * Answer the {@link MailSender} associated with the engine
	 */
	public MailSender getMailSender() {
		if (mailSender == null && hasValidSmtpConfig()) {
			Mailer mailer = new Mailer(
				new ServerConfig(smtpHost, smtpPort, smtpUsername, smtpPassword),
				TransportStrategy.SMTP_TLS);
			mailer.setDebug(emailDebug);
			log.info("Creating mail sender with rate limit of {}", getEmailRateLimit());
			mailSender = new MailerMailSender(mailer, getEmailRateLimit());
		}
		return mailSender;
	}

	// TCM: For testing purposes
	protected void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	public JobQueue<NotificationData> getActiveQueue() {
		if (activeQueue == null) {
			activeQueue = new InMemoryJobQueue<>();
		}
		return activeQueue;
	}
	protected void setActiveQueue(JobQueue<NotificationData> queue) { this.activeQueue = queue; }

	public JobQueue<NotificationData> getDeadQueue() {
		if (deadQueue == null) {
			deadQueue = new InMemoryJobQueue<>();
		}
		return deadQueue;
	}
	protected void setDeadQueue(JobQueue<NotificationData> deadQueue) { this.deadQueue = deadQueue; }

	protected EntityManager getEntityManager() { return entityManager; }
	protected void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	// DERIVED

	/**
	 * Answer the name to use when sending an email notification, or default if none configured.
	 */
	protected String getEmailName() {
		return (emailName == null || emailName.isEmpty()) ? DEFAULT_EMAIL_NAME : emailName;
	}

	/**
	 * Answer the email requeue limit (the number of times a failing notification will be
	 * placed back on the notification queue), or default, if none configured.
	 */
	protected int getEmailRequeueLimit() {
		return (emailRequeueLimit == null) ? DEFAULT_EMAIL_REQUEUE_LIMIT : emailRequeueLimit;
	}

	/**
	 * Answer the rate limit (per second), or default, if none configured.
	 */
	protected int getEmailRateLimit() {
		return (emailRateLimit == null) ? DEFAULT_EMAIL_RATE_LIMIT : emailRateLimit;
	}

	// SCHEDULING

	public static final String CONTEXT_KEY = "_context";
	public static final String TARGET_KEY = "_target";
	public static final String RECIPIENT_KEY = "_recipient";

	/**
	 * Schedule a notification to be sent for the specified target/context with the specified timing
	 */
	public void scheduleNotification(
			Notification notification,
			BaseObject target,
			Organization context,
			Timing timing)
	{
		if (timing == null || timing == Timing.NONE) {
			log.info("Timing value of NONE submitted.  Notification for {} of {} will not be sent.", target.toString(), notification.getTemplateName());
		} else {
			getActiveQueue().pushFor(new NotificationData(notification, target, context), timing);
			log.debug("Scheduled {} notification ({}) to target {}",
				timing.toString(), notification.getTemplateName(), target);
		}
	}

	/**
	 * Unschedule the specified notification for the specified target/context
	 */
	public void unscheduleNotification(Notification notification, BaseObject target, Organization context) {
		getActiveQueue().popFor(new NotificationData(notification, target, context));
		log.debug(String.format("Unscheduled notification (%s) to target %s", notification.getTemplateName(), target));
	}

	/**
	 * Trigger to send all scheduled notifications (regardless of timing)
	 */
	public void sendScheduledNotifications() {
		sendScheduledNotifications(true);
	}

	/**
	 * Trigger to send any notifications (regardless of timing), requeing on failure as specified.
	 */
	public void sendScheduledNotifications(boolean requeue) {
		sendScheduledNotifications(Timing.IMMEDIATE, requeue);
		sendScheduledNotifications(Timing.HOUR, requeue);
		sendScheduledNotifications(Timing.DAY, requeue);
	}

	/**
	 * Trigger to send any scheduled notifications with the specified timing
	 */
	public void sendScheduledNotifications(Timing timing) {
		sendScheduledNotifications(timing, true);
	}

	/**
	 * Trigger to send any scheduled notifications with the specified timing, requeing on failure as specified
	 */
	public void sendScheduledNotifications(Timing timing, boolean requeue) {
		Collection<Job<NotificationData>> jobs = getActiveQueue().popFor(timing);

		// Exit quickly if there are not jobs currently for the specified timing
		if (jobs.isEmpty()) return;

		log.info(String.format("Sending %d scheduled notification(s) (timing: %s)", jobs.size(), timing.toString()));
		Set<Job<NotificationData>> retryJobs = new LinkedHashSet<>();
		Set<Job<NotificationData>> deadJobs = new LinkedHashSet<>();
		for (Job<NotificationData> job : jobs) {
			try {
				NotificationData notificationData = job.getData().inflate(getEntityManager());
				sendNotification(notificationData.getNotification(), notificationData.getTarget(), notificationData.getContext());
			}
			catch (Throwable t) {
				log.warn("Unable to send notification", t);
				job.touch();		// TCM: Make sure to 'touch' the job, so JPA sees it as changed.
				if ((t instanceof NotificationException) && ((NotificationException) t).isRetryable()) {
					job.getData().markRequeued(t);
					if (job.getData().getRequeueCount() < getEmailRequeueLimit()) {
						job.increaseFrequency();
						retryJobs.add(job);
					}
					else {
						deadJobs.add(job);
					}
				}
				else {
					job.getData().markFailed(t);
					deadJobs.add(job);
				}
			}
		}

		// If not requeing, move any retry entries to the fail entries
		if (!requeue) {
			deadJobs.addAll(retryJobs);
			retryJobs.clear();
		}

		// Retry jobs that we can...
		if (!retryJobs.isEmpty()) {
			for (Job<NotificationData> retryJob : retryJobs) {
				getActiveQueue().push(retryJob);
			}
			log.info(String.format("Requeued %d failed notifications", retryJobs.size()));
		}

		// ...otherwise, push failing jobs to the dead queue
		if (!deadJobs.isEmpty()) {
			for (Job<NotificationData> failJob : deadJobs) {
				getDeadQueue().push(failJob);
			}
			log.info(String.format("Moved %d failed notifications to dead queue", deadJobs.size()));
		}
	}

	/**
	 * Send the specified notification to the specified target on behalf of the specified context.
	 * <p/>
	 * Note: the expectation is that this method is being called in a background thread (via a scheduled executor).
	 * As such, this method will send notifications synchronously, to allow for exceptions to propagate to the
	 * caller (and perform proper handling/requeuing/etc.).<br/>
	 * (BPD-1708) This is also important as the {@link MailSender} may rate limit the e-mail's being sent,
	 * effectively blocking the thread when waiting for limits to clear.
	 */
	protected void sendNotification(
			Notification notification,
			BaseObject target,
			Organization context)
		throws NotificationException
	{
		// Determine the targeted users
		List<User> users = getUsers(target);

		// Diagnostic info
		String targetInfo = (target == null)
				? "(unspecified target)"
				: String.format("%s(%s)", target.getClass().getSimpleName(), getDisplayName(target));
		String contextInfo = (context == null) ? "(unspecified context)" : context.getDisplayName();

		if (users.isEmpty()) {
			log.warn(String.format("No users associated with target %s", getDisplayName(target)));
		}
		else if (getMailSender() == null) {
			NotificationException e = new NotificationException(
				String.format("Unable to configure mail session; notification %s not sent to %s for %s",
					notification.getTemplateName(),
					targetInfo,
					contextInfo));
			e.setRetryable(true);
			throw e;
		}
		else {
			// Generate emails for all users
			List<Email> emails = new ArrayList<>();
			for (User user : users) {
				Map<String, Object> notificationBindings = new HashMap<>();
				// Ensure that the context, target and recipient are made available in the bindings
				notificationBindings.put(CONTEXT_KEY, context);
				if (context == null) {
					log.warn(String.format("Sending notification %s with no context", notification.getTemplateName()));
				}
				notificationBindings.put(TARGET_KEY, target);
				if (target == null) {
					log.warn(String.format("Sending notification %s with no target", notification.getTemplateName()));
				}
				notificationBindings.put(RECIPIENT_KEY, user);
				Map<?, ?> templateBindings = notification.applyTemplate(notificationBindings);

				Email email = asEmail(user, notification, target, context);
				String subject = getBindingAsString(templateBindings, "subject");
				if (StringUtils.isEmpty(subject)) {
					log.warn(String.format("No subject defined when applying template %s", notification.getTemplateName()));
					// BPD-2076: Sonar bug; subject cannot be null
					subject = DEFAULT_SUBJECT;
				}
				String htmlContent = getBindingAsString(templateBindings, "htmlContent");
				if (StringUtils.isEmpty(htmlContent)) {
					log.warn(String.format("No HTML content defined when applying template %s", notification.getTemplateName()));
				}
				String content = getBindingAsString(templateBindings, "content");
				if (StringUtils.isEmpty(content)) {
					log.warn(String.format("No content defined when applying template %s", notification.getTemplateName()));
				}
				email.setSubject(subject);
				email.setTextHTML(htmlContent);
				email.setText(content);

				emails.add(email);
			}

			// TCM: Somewhat redundant (as this is called internally in SimpleMail API), but this allows us to catch
			// any mailing exceptions and capture then as (non-retryable) Notification Exceptions
			for (Email email : emails) {
				try {
					getMailSender().validate(email);
				}
				catch (MailException e) {
					throw new NotificationException(
						String.format("Invalid email generated by notification (%s) sent to %s for %s",
							notification.getTemplateName(), targetInfo, contextInfo), e);
				}
			}

			// BPD-1619: Send all the e-mails only after we are sure binding works for all users
			for (Email email : emails) {
				StringBuilder recipientAddresses = new StringBuilder();
				int index = 0;
				for (Recipient recipient : email.getRecipients()) {
					if (index > 0) {
						recipientAddresses.append(", ");
					}
					recipientAddresses.append(recipient.getAddress());
				}
				try {
					getMailSender().sendMail(email);
				}
				catch (MailException e) {
					NotificationException ne = new NotificationException(
						String.format("Unable to send notification (%s) to %s", notification.getTemplateName(), recipientAddresses), e);
					ne.setRetryable(true);
					throw ne;
				}
				log.debug(String.format("Sent notification (%s) to %s", notification.getTemplateName(), recipientAddresses));
			}
			log.info(String.format("Sent notification (%s) to %d users", notification.getTemplateName(), users.size()));
		}
	}

	// LIFECYCLE

	// TCM: In *theory*, we can/should use the executor service that is bound in JNDI by Wildfly.
	// However, in practice, there are classpath issues with stopping/starting runnables on this service from the
	// JMX thread.  These issues do not manifest with an explicitly created executor.
	// @Resource
	// private ManagedScheduledExecutorService managedScheduleService;
	private ScheduledExecutorService scheduleService;

	private ScheduledFuture<?> immediateFuture;
	private ScheduledFuture<?> hourlyFuture;
	private ScheduledFuture<?> dailyFuture;

	private static final String MBEAN_NAME = "com.baseventure.core.notifications:type=NotificationEngine";

	@Override
	public void onStartup() {
		// Register an MBean
		try {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			ObjectName engineName = new ObjectName(MBEAN_NAME);

			mbs.registerMBean(this, engineName);
			log.debug("Registered NotificationEngine MBean");
		} catch (MalformedObjectNameException
				| InstanceAlreadyExistsException
				| MBeanRegistrationException
				| NotCompliantMBeanException e) {
			log.error("Unable to register NotificationEngine MBean", e);
		}

		scheduleService = Executors.newSingleThreadScheduledExecutor();

		// TCM: See comments on managedScheduleService above
		// Determine the schedule service to use
		// if (managedScheduleService == null) {
		//	scheduleService = Executors.newSingleThreadScheduledExecutor();
		//	log.info("Unable to find managed scheduled service; creating new service for notification");
		// }
		// else {
		//	scheduleService = managedScheduleService;
		//	log.info("Using managed scheduled service for notification");
		// }

		// Start sending scheduled notifications
		start();
	}

	@Override
	public void onShutdown() {
		// Stop sending scheduled notifications
		stop();

		// If we are not using the managed schedule service, shut it down
		// if (scheduleService != null && scheduleService != managedScheduleService) {
		if (scheduleService != null) {
			scheduleService.shutdown();
			scheduleService = null;
			log.debug("Shutdown scheduled service");
		}

		try {
			MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			ObjectName engineName = new ObjectName(MBEAN_NAME);

			// Deregister an existing MBean
			if (mbs.isRegistered(engineName)) {
				mbs.unregisterMBean(engineName);
			}
			log.debug("Deregistered NotificationEngine MBean");
		} catch (MalformedObjectNameException | MBeanRegistrationException | InstanceNotFoundException e) {
			log.error("Unable to deregister NotifictionEngine MBean", e);
		}
	}

	/**
	 * Start the sending of scheduled notifications
	 */
	public void start() {
		Calendar now = Calendar.getInstance();
		int minuteDelay = 60 - now.get(Calendar.MINUTE);
		int hourDelay = 23 - now.get(Calendar.HOUR_OF_DAY);

		log.info("Starting notification tasks");
		if (immediateFuture == null) {
			// Start a scheduled service to go off every minute
			immediateFuture = scheduleService.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					try {
						log.debug("Triggering immediate notifications");
						NotificationEngine.this.sendScheduledNotifications(Timing.IMMEDIATE);
					} catch (Throwable t) {
						log.error("Error triggering immediate notifications", t);
					}
				}
			}, 0, 1, TimeUnit.MINUTES);
			log.info("Started immediate notification task");
		}

		if (hourlyFuture == null) {
			// Start a scheduled service to go off hourly
			// Determine the minutes to the next hour
			hourlyFuture = scheduleService.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					try {
						log.debug("Triggering hourly notifications");
						NotificationEngine.this.sendScheduledNotifications(Timing.HOUR);
					} catch (Throwable t) {
						log.error("Error triggering hourly notifications", t);
					}
				}
			}, minuteDelay, 60, TimeUnit.MINUTES);
			log.info("Started hourly notification task");
		}

		if (dailyFuture == null) {
			// Start a scheduled service to go off daily
			dailyFuture = scheduleService.scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					try {
						log.debug("Triggering daily notifications");
						NotificationEngine.this.sendScheduledNotifications(Timing.DAY);
					} catch (Throwable t) {
						log.error("Error triggering daily notifications", t);
					}
				}
			}, (hourDelay * 60) + minuteDelay, (long) 60*24, TimeUnit.MINUTES);
			log.info("Started daily notification task");
		}
	}

	/**
	 * Stop the sending of scheduled notifications
	 */
	public void stop() {
		log.info("Stopping notification tasks");
		if (immediateFuture != null) {
			immediateFuture.cancel(false);
			immediateFuture = null;
			log.info("Canceled immediate notification task");
		}
		if (hourlyFuture != null) {
			hourlyFuture.cancel(false);
			hourlyFuture = null;
			log.info("Canceled hourly notification task");
		}
		if (dailyFuture != null) {
			dailyFuture.cancel(false);
			dailyFuture = null;
			log.info("Canceled daily notification task");
		}
	}

	/**
	 * Restart the sending of scheduled notifications
	 */
	public void restart() {
		stop();
		start();
	}

	// MANAGEMENT

	@Inject
	private UserTransaction userTransaction;

	/**
	 * Answer the number of notifications queued for hourly timing
	 */
	public int getHourlyQueueSize() {
		return (activeQueue == null) ? 0 : activeQueue.peekFor(Timing.HOUR).size();
	}

	/**
	 * Answer the number of notifications queued for daily timing
	 */
	public int getDailyQueueSize() {
		return (activeQueue == null) ? 0 : activeQueue.peekFor(Timing.DAY).size();
	}

	/**
	 * Flush any scheduled notifications immediately
	 */
	public void flushNotifications() throws Exception {
		flushNotifications(true);
	}

	/**
	 * Flush any scheduled notifications immediately, requeuing failures as specified
	 */
	public void flushNotifications(boolean requeue) throws Exception {
		if (userTransaction != null) {
			log.info("Sending scheduled notifications");
			try {
				userTransaction.begin();
				sendScheduledNotifications(requeue);
			} catch (SecurityException | IllegalStateException e) {
				log.error(String.format("Unable to send notifications: %s", e.getMessage()));
				throw e;
			} finally {
				userTransaction.commit();
			}
		}
		else {
			log.warn("Unable to access user transaction");
		}
	}

	// UTILITY

	/**
	 * Answer the users associated with the specified target
	 */
	protected static List<User> getUsers(BaseObject target) {
		// Logic to derive users...  removed as simplification
		return new ArrayList<>();
	}

	/**
	 * Answer a display name for the specified target
	 */
	protected static String getDisplayName(BaseObject target) {
		// Case: no target
		if (target == null) { return "(unspecified)"; }

		// Additional cases removed as simplification
		// ...

		return target.getUuid().toString();
	}

	/**
	 * Answer the specified notification to the specified recipient/target/context as an {@link Email}.
	 */
	protected Email asEmail(User recipient, Notification notification, BaseObject target, Organization context)
		throws NotificationException
	{
		if (context == null) {
			log.warn(String.format("Attempting to send notification to %s with no context specified", recipient.getLogin()));
		}

		Email email = new Email();
		String fromName = (context == null ? getEmailName() : context.getName());
		String fromAddress = String.format("%s@%s",
			// BPD-1635: send engine-generated notifications from a common email name
			getEmailName(),
			(emailDomain == null ? "" : emailDomain)
		);
		if (EmailAddressValidator.isValid(fromAddress)) {
			email.setFromAddress(fromName, fromAddress);
		}
		else {
			throw new NotificationException(String.format("Unable to send email; invalid generated FROM address: %s", fromAddress));
		}

		// BPD-1635: Add some fundmanager-specific headers to facilitate identifying the context (if present)
		if (context != null) {
			email.addHeader("X-fundmgr-org-name", context.getName());
			email.addHeader("X-fundmgr-org-id", context.getUuid());
		}

		if (context != null
			&& context.getPrimaryContact() != null
			&& context.getPrimaryContact().getPrimaryEmail() != null) {
			email.setReplyToAddress(context.getPrimaryContact().getFullName(), context.getPrimaryContact().getPrimaryEmail());
		}
		email.addNamedToRecipients(recipient.getFullName(), recipient.getLogin());

		for (Notification.Resource embeddedImageResource : notification.getEmbeddedImages()) {
			email.addEmbeddedImage(embeddedImageResource.getName(), embeddedImageResource.getData(), embeddedImageResource.getType());
		}
		for (Notification.Resource attachmentResource : notification.getAttachments()) {
			email.addAttachment(attachmentResource.getName(), attachmentResource.getData(), attachmentResource.getType());
		}

		return email;
	}

	protected static String getBindingAsString(Map<?, ?> bindings, String key) {
		Object value = bindings.get(key);
		return (value == null) ? null : value.toString();
	}
}
