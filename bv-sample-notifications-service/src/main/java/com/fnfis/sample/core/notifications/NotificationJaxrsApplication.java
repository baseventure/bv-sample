package com.fnfis.sample.core.notifications;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * A marker implementation of a JAX-RS {@link Application}.
 */
@ApplicationPath(NotificationJaxrsApplication.PATH)
public class NotificationJaxrsApplication extends Application {
	/**
	 * The URL path to address services that are part of this application.
	 */
	public static final String PATH = "/notification/api/v1";
}
