package com.fnfis.sample.core.schedule;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

/**
 * A unit of work to be placed on a {@link JobQueue}
 */
@Entity
public class Job<T extends Serializable> {
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column(nullable=false)
	private String type;

	@Lob
	@Type(type="java.io.Serializable")
	@Column(length=10485760, nullable=true)	// Support up to 10MB (for attachments/embedded images)
	private T data;

	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private Timing timing;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	protected Date updatedOn;

	// CONSTRUCTORS

	public Job() {
		super();
		touch();
	}
	public Job(T data) {
		this();
		setData(data);
	}
	public Job(T data, Timing timing) {
		this(data);
		setTiming(timing);
	}

	// GETTERS/SETTERS

	public Long getId() { return id; }
	protected void setId(Long id) { this.id = id; }

	public String getType() { return type; }
	public void setType(String type) { this.type = type; }

	public T getData() { return data; }
	public void setData(T data) { this.data = data; }

	public Timing getTiming() { return timing; }
	public void setTiming(Timing timing) { this.timing = timing; }

	// UTILITY

	public void touch() { updatedOn = new Date(); }

	/**
	 * Increase the timing of this job to the next logical frequency (if possible)
	 */
	public void increaseFrequency() {
		setTiming(getTiming().faster());
	}

	/**
	 * Decrease the timing of this job to the next logical frequency (if possible)
	 */
	public void decreaseFrequency() {
		setTiming(getTiming().slower());
	}
}
