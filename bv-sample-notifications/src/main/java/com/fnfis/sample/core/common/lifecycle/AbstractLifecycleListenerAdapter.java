package com.fnfis.sample.core.common.lifecycle;

/**
 * Default, empty implementation of {@link LifecycleListener}.
 * Intended as a supertype for implementations that don't want to implement all
 * callbacks.
 */
public abstract class AbstractLifecycleListenerAdapter implements LifecycleListener {

	@Override
	public void onStartup() {}

	@Override
	public void onShutdown() {}
}
