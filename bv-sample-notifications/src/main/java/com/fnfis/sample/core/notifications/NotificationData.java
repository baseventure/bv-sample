package com.fnfis.sample.core.notifications;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;

import javax.persistence.EntityManager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fnfis.sample.core.model.BaseObject;
import com.fnfis.sample.core.model.Organization;
import com.fnfis.sample.core.model.TypedObjectReference;
import com.fnfis.sample.core.model.utils.query.CriteriaQueries;

/**
 * Representation of a queued notification
 */
public class NotificationData implements Externalizable {
	private static final long serialVersionUID = 2L;

	private Notification notification;
	@JsonIgnore
	private BaseObject target;
	@JsonIgnore
	private Organization context;

	@JsonProperty("requeue-count")
	private int requeueCount;
	@JsonProperty("failure-reason")
	private String failureReason;
	@JsonProperty("stack-trace")
	private String stackTrace;

	// The following are used for equality and data serialization
	@JsonIgnore
	private TypedObjectReference targetRef;
	@JsonIgnore
	private TypedObjectReference contextRef;

	// CONSTRUCTORS

	public NotificationData() { super(); }
	public NotificationData(Notification notification, BaseObject target, Organization context) {
		this();
		setNotification(notification);
		setTarget(target);
		setContext(context);
	}
	// GETTER/SETTER

	public Notification getNotification() {return notification; }
	public void setNotification(Notification notification) { this.notification = notification; }
	public BaseObject getTarget() { return target; }
	public void setTarget(BaseObject context) {
		this.target = context;
		this.targetRef = null;	// Reset context ref
	}
	public Organization getContext() { return context; }
	public void setContext(Organization context) {
		this.context = context;
		this.contextRef = null;	// Reset owner ref
	}

	protected TypedObjectReference getTargetRef() {
		if (targetRef == null && target != null) {
			targetRef = new TypedObjectReference(target);
		}
		return targetRef;
	}
	protected TypedObjectReference getContextRef() {
		if (contextRef == null && context != null) {
			contextRef = new TypedObjectReference(context);
		}
		return contextRef;
	}

	public int getRequeueCount() { return requeueCount; }
	protected void setRequeueCount(int count) { this.requeueCount = count; }
	public String getFailureReason() { return failureReason; }
	public void setFailureReason(String reason) { this.failureReason = reason; }
	public String getStackTrace() { return stackTrace; }
	protected void setStackTrace(String stackTrace) { this.stackTrace = stackTrace; }

	// DERIVED

	@JsonProperty("target-name")
	public String getTargetName() {
		return (getTarget() == null) ? "(unspecified)" : getTarget().getDisplayName();
	}

	@JsonProperty("context-name")
	public String getContextName() {
		return (getContext() == null) ? "(unspecified)" : getContext().getDisplayName();
	}

	// UTILITY

	public void merge(NotificationData data) {
		if (getNotification() == null) {
			setNotification(data.getNotification());
		}
		else {
			getNotification().merge(data.getNotification());
		}
	}

	/**
	 * Mark the data as having failed for the specified reason
	 */
	public void markFailed(Throwable reason) {
		setFailureReason(reason.getMessage());
		StringWriter writer = new StringWriter();
		reason.printStackTrace(new PrintWriter(writer));
		writer.flush();
		setStackTrace(writer.toString());
	}
	/**
	 * Mark the data as having been requeued for the specified reason
	 */
	public void markRequeued(Throwable reason) {
		markFailed(reason);
		setRequeueCount(getRequeueCount() + 1);
	}

	// OVERRIDE

	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) { return false; }
		NotificationData rhs = (NotificationData) obj;
		return notification.matches(rhs.notification)
			&& Objects.equals(getTargetRef(), rhs.getTargetRef())
			&& Objects.equals(getContextRef(), rhs.getContextRef());
	}

	@Override
	public int hashCode() {
		return (notification == null || notification.getTemplateName() == null ? 0 : notification.getTemplateName().hashCode())
			+ (getTargetRef() == null ? 0 : getTargetRef().hashCode())
			+ (getContextRef() == null ? 0 : getContextRef().hashCode());
	}

	// SERIALIZATION

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(serialVersionUID);
		out.writeObject(getNotification());
		out.writeObject(getTargetRef());
		out.writeObject(getContextRef());

		out.writeInt(getRequeueCount());
		out.writeObject(getFailureReason());
		out.writeObject(getStackTrace());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		long version = in.readLong();

		if (version >= 1L) {
			setNotification((Notification) in.readObject());
			targetRef = (TypedObjectReference) in.readObject();
			contextRef = (TypedObjectReference) in.readObject();
		}
		if (version >= 2L) {
			setRequeueCount(in.readInt());
			setFailureReason((String)in.readObject());
			setStackTrace((String)in.readObject());
		}
	}

	// UTILITY

	/**
	 * Inflate the {@link TypedObjectRef}s (if any) using the specified entity manager.
	 */
	public NotificationData inflate(EntityManager entityManager) {
		if (getTarget() == null && targetRef != null) {
			setTarget(inflate(targetRef, entityManager));
		}
		if (getContext() == null && contextRef != null) {
			setContext(inflate(contextRef, entityManager));
		}
		return this;
	}

	protected <T extends BaseObject> T inflate(TypedObjectReference ref, EntityManager entityManager) {
		return ref == null ? null : CriteriaQueries.findReferencedObject(entityManager, ref);
	}

}
