package com.fnfis.sample.core.schedule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

public abstract class PersistentJobQueue<T extends Serializable> implements JobQueue<T> {
	@PersistenceContext(unitName = "bv-core-pu")
	private EntityManager entityManager;
	
	// QUERIES
	
	private static final String FIND_JOBS_QUERY = "SELECT j FROM Job AS j WHERE j.type = :type";
	private static final String FIND_JOBS_BY_TIMING_QUERY = "SELECT j FROM Job AS j WHERE j.type = :type AND j.timing = :timing";
		
	// GETTERS/SETTERS
	
	public EntityManager getEntityManager() { return entityManager; }
	public void setEntityManager(EntityManager entityManager) { this.entityManager = entityManager; }
	
	public abstract String getType();

	// INTERNAL
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Job<T>> getJobs() {
		return entityManager
			.createQuery(FIND_JOBS_QUERY)
			.setLockMode(LockModeType.PESSIMISTIC_WRITE)
			.setParameter("type", getType())
			.getResultList();
	}

	// REQUIRED
	
	@Override
	public Job<T> push(Job<T> job) {
		job.setType(getType());
		entityManager.persist(job);
		return job;
	}
	
	@Override
	public Job<T> pushFor(T data, Timing timing) {
		return push(new Job<T>(data, timing));
	}

	@Override
	public Collection<Job<T>> pushAllFor(Collection<T> data, Timing timing) {
		Collection<Job<T>> jobs = new ArrayList<Job<T>>();
		for (T datum : data) {
			jobs.add(pushFor(datum, timing));
		}
		return jobs;
	}

	@Override
	public Collection<Job<T>> peekFor(T data) {
		// Pretty sure a query based on serialization data match will not work, so this is the old fashioned way
		List<Job<T>> matches = new LinkedList<>();
		for (Job<T> job : getJobs()) {
			if (job.getData().equals(data)) {
				matches.add(job);
			}
		}
		return matches;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Job<T>> peekFor(Timing timing) {
		return entityManager
			.createQuery(FIND_JOBS_BY_TIMING_QUERY)
			.setLockMode(LockModeType.PESSIMISTIC_WRITE)
			.setParameter("type", getType())
			.setParameter("timing", timing)
			.getResultList();
	}

	@Override
	public Collection<Job<T>> popFor(T data) {
		Collection<Job<T>> matches = peekFor(data);
		for (Job<T> job : matches) {
			remove(job);
		}
		return matches;
	}

	@Override
	public Collection<Job<T>> popFor(Timing timing) {
		Collection<Job<T>> matches = peekFor(timing);
		for (Job<T> match : matches) {
			remove(match);
		}
		return matches;
	}

	@Override
	public Collection<Job<T>> clear() {
		Collection<Job<T>> jobs = getJobs();
		for (Job<T> job : jobs) {
			remove(job);
		}
		return jobs;
	}
	
	// INTERNAL
	
	protected void remove(Job<T> job) {
		entityManager.remove(job);
	}
}
