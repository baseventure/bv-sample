package com.fnfis.sample.core.notifications;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fnfis.sample.core.model.User;
import com.fnfis.sample.core.schedule.JobQueue;
import com.fnfis.sample.core.schedule.Timing;

/**
 * JAX-RS service for interacting with notifications
 */
@Path("/")
@ApplicationScoped
public class NotificationJaxrsService {

	private static final Logger log = LoggerFactory.getLogger(NotificationJaxrsService.class);

	private static final String ENGINE_PATH = "/engine";
	private static final String QUEUE_PATH = ENGINE_PATH + "/queue/{queue-name}";
	private static final String SIMULATOR_PATH = ENGINE_PATH + "/simulator";

	private static final String ACTION_FLUSH = "flush";
	private static final String ACTION_FORCE_FLUSH = "force-flush";

	// INJECTION

	@Inject
	private NotificationEngine notificationEngine;

	@PersistenceContext(unitName = "bv-core-pu")
	private EntityManager entityManager;

	// LIFECYCLE

	// DOWNLOAD

	/**
	 * Provide information about the engine.
	 */
	@GET
	@Path(ENGINE_PATH)
	@Produces({ MediaType.APPLICATION_JSON })
	@Transactional(Transactional.TxType.REQUIRED)
	public Response getEngineData()
	{
		return Response
			.ok()
			.entity(new NotificationEngineData(notificationEngine).inflate(entityManager))
			.build();
	}

	/**
	 * Perform an action against the engine.  Allowable actions are:
	 * <ul>
	 * <li>{@code flush}</li>
	 * <li>{@code force-flush}</li>
	 * </ul>
	 */
	@PUT
	@Path(ENGINE_PATH)
	@Produces({ MediaType.APPLICATION_JSON })
	@Transactional(Transactional.TxType.REQUIRED)
	public Response engineAction(@QueryParam("action") String action) {
		log.debug(String.format("Attempting to perform notification action: %s", action));
		Status status = Response.Status.NOT_FOUND;
		if (!StringUtils.isEmpty(action)) {
			if (ACTION_FLUSH.equals(action)) {
				notificationEngine.sendScheduledNotifications();
				status = Response.Status.OK;
			}
			else if (ACTION_FORCE_FLUSH.equals(action)) {
				notificationEngine.sendScheduledNotifications(false);
				status = Response.Status.OK;
			}
			else {
				log.warn(String.format("Request to perform unknown action: '%s'", action));
			}
		}

		return Response
			.status(status)
			.entity(new NotificationEngineData(notificationEngine).inflate(entityManager))
			.build();
	}

	/**
	 * Provide information about the specified queue
	 */
	@GET
	@Path(QUEUE_PATH)
	@Produces({ MediaType.APPLICATION_JSON })
	@Transactional(Transactional.TxType.REQUIRED)
	public Response getQueueData(@PathParam("queue-name") String queueName)
	{
		return Response
			.ok()
			.entity(new NotificationQueueData(getQueueNamed(queueName)).inflate(entityManager))
			.build();
	}

	/**
	 * Clear the specified queue
	 */
	@DELETE
	@Path(QUEUE_PATH)
	@Produces({ MediaType.APPLICATION_JSON })
	@Transactional(Transactional.TxType.REQUIRED)
	public Response clearQueue(@PathParam("queue-name") String queueName) {
		JobQueue<NotificationData> queue = getQueueNamed(queueName);
		Status status;
		if (queue == null) {
			status = Response.Status.NOT_FOUND;
		}
		else {
			queue.clear();
			status = Response.Status.OK;
		}

		return Response
			.status(status)
			.entity(new NotificationQueueData(queue).inflate(entityManager))
			.build();
	}

	// TESTING

	@Inject
	private User currentUser;

	/**
	 * Schedule a simulated failure (by attempting to send a notification with an
	 * invalid template)
	 */
	@POST
	@Path(SIMULATOR_PATH)
	@Transactional(Transactional.TxType.REQUIRED)
	public Response scheduleFailure(
			@QueryParam("retryable") boolean retryable)
	{
		Notification notification = new Notification("invalid.groovy");
		if (retryable) {
			notification.useRetryTemplate();
		}
		notificationEngine.scheduleNotification(notification, currentUser, null, Timing.HOUR);
		return Response.ok().build();
	}

	// UTILITY

	/**
	 * Answer the {@link JobQueue} with the specified name
	 */
	protected JobQueue<NotificationData> getQueueNamed(String queueName) {
		switch (queueName) {
			case NotificationEngine.ACTIVE_QUEUE_NAME:
				return notificationEngine.getActiveQueue();
			case NotificationEngine.DEAD_QUEUE_NAME:
				return notificationEngine.getDeadQueue();
			default:
				return null;
		}
	}
}
