package com.fnfis.sample.core.notifications;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.Serializable;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.fnfis.sample.core.model.utils.DateUtils;

import groovy.lang.GString;

public class NotificationTestCase {
	@Test
	public void testDefault() {
		String templateName = "template.groovy";
		Notification n = new Notification(templateName);
		assertThat(n, is(not(nullValue())));
		assertThat(n.getTemplateName(), is(templateName));
		assertThat(n.getSubstitutions(), is(not(nullValue())));
		assertTrue(n.getSubstitutions().isEmpty());
		assertThat(n.getAttachments(), is(not(nullValue())));
		assertTrue(n.getAttachments().isEmpty());
		assertThat(n.getEmbeddedImages(), is(not(nullValue())));
		assertTrue(n.getEmbeddedImages().isEmpty());
	}

	@Test
	public void testMatch() {
		Notification n1 = new Notification("user_enabled.groovy");
		Notification n2 = new Notification("user_enabled.groovy");
		assertTrue(n1.matches(n2));
	}

	@Test
	public void testSubstitution() {
		Notification n = new Notification("<ignored>");

		String key = "key";
		String value1 = "value1";
		Integer value2 = 2;

		// Add
		n.addSubstitution(key, value1);
		assertThat(n.getSubstitution(key), is(value1));

		// Replace
		n.addSubstitution(key, value2);
		assertThat(n.getSubstitution(key), is(value2));

		// Datatypes
		boolean booleanValue = true;
		char charValue = 'c';
		int intValue = 1234;
		long longValue = 1234L;
		float floatValue = 12.34F;
		double doubleValue = 12.34;
		String stringValue = "string";
		Date dateValue = new Date();		// Represents a Serializable object

		n.addSubstitution("boolean", booleanValue);
		n.addSubstitution("char", charValue);
		n.addSubstitution("int", intValue);
		n.addSubstitution("long", longValue);
		n.addSubstitution("float", floatValue);
		n.addSubstitution("double", doubleValue);
		n.addSubstitution("string", stringValue);
		n.addSubstitution("date", dateValue);

		n.addSubstitution("booleanArray", new boolean[] { booleanValue });
		n.addSubstitution("BooleanArray", new Boolean[] { booleanValue });
		n.addSubstitution("charArray", new char[] { charValue });
		n.addSubstitution("charArray", new Character[] { charValue });
		n.addSubstitution("intArray", new int[] { intValue });
		n.addSubstitution("intArray", new Integer[] { intValue });
		n.addSubstitution("longArray", new long[] { longValue });
		n.addSubstitution("longArray", new Long[] { longValue });
		n.addSubstitution("floatArray", new float[] { floatValue });
		n.addSubstitution("floatArray", new Float[] { floatValue });
		n.addSubstitution("doubleArray", new double[] { doubleValue });
		n.addSubstitution("doubleArray", new Double[] { doubleValue });
		n.addSubstitution("stringArray", new String[] { stringValue });
		n.addSubstitution("dateArray", new Date[] { dateValue });

		ArrayList<Object> objects = new ArrayList<>();
		objects.add(booleanValue);
		objects.add(charValue);
		objects.add(intValue);
		objects.add(longValue);
		objects.add(floatValue);
		objects.add(doubleValue);
		objects.add(stringValue);
		objects.add(dateValue);
		n.addSubstitution("objectsCollection", objects);

		HashMap<String, Object> objectsMap = new HashMap<>();
		objectsMap.put("boolean", booleanValue);
		objectsMap.put("char", charValue);
		objectsMap.put("int", intValue);
		objectsMap.put("long", longValue);
		objectsMap.put("float", floatValue);
		objectsMap.put("double", doubleValue);
		objectsMap.put("string", stringValue);
		objectsMap.put("date", dateValue);
		n.addSubstitution("objectsMap", objectsMap);

		// Invalid
		Object[] badArray = new Object[] { new Object() };
		try {
			n.addSubstitution("bad", badArray);
			fail("Unexpected success adding array with non-serializable content");
		} catch (IllegalArgumentException e) { /* IGNORE */ }

		ArrayList<Object> badList = new ArrayList<>();
		badList.add(new Object());
		try {
			n.addSubstitution("bad", badList);
			fail("Unexpected success adding list with non-serializable content");
		} catch (IllegalArgumentException e) { /* IGNORE */ }

		HashMap<String, Object> badMap = new HashMap<>();
		badMap.put("badObject", new Object());
		try {
			n.addSubstitution("bad", badMap);
			fail("Unexpected success adding map with non-serializable values");
		} catch (IllegalArgumentException e) { /* IGNORE */ }
	}

	@Test
	public void testMergeBasic() {
		boolean baseBoolean = true;
		char baseChar = 'b';
		long baseLong = 1234L;
		double baseDouble = 12.34;
		String baseString = "base";
		TestSubstitutable baseObject = new TestSubstitutable(1);
		baseObject.name = baseString;
		baseObject.dates.add(DateUtils.startOf(Year.now().getValue()));
		baseObject.lookup.put("base", baseString);
		baseObject.lookup.put("shared", baseString);

		Notification base = new Notification("<test>");
		base.addSubstitution("boolean", baseBoolean);
		base.addSubstitution("char", baseChar);
		base.addSubstitution("long", baseLong);
		base.addSubstitution("double", baseDouble);
		base.addSubstitution("string", baseString);
		base.addSubstitution("object", baseObject);
		base.addAttachment("attachment", new byte[0], "application/octet-stream");

		// Merging of non-matching notifications is an error
		Notification addition = new Notification("<addition>");
		try {
			base.merge(addition);
			fail("Unexpected success merging notifications");
		} catch (IllegalArgumentException e) { /* IGNORE */ }

		boolean addBoolean = false;
		char addChar = 'a';
		long addLong = 4321L;
		double addDouble = 43.21;
		String addString = "addition";
		TestSubstitutable addObject = new TestSubstitutable(1);
		addObject.name = addString;
		addObject.dates.add(DateUtils.endOf(Year.now().getValue()));
		addObject.lookup.put("add", addString);
		addObject.lookup.put("shared", addString);

		addition = new Notification("<test>");
		addition.addSubstitution("boolean", addBoolean);
		addition.addSubstitution("char", addChar);
		addition.addSubstitution("long", addLong);
		addition.addSubstitution("double", addDouble);
		addition.addSubstitution("string", addString);
		addition.addSubstitution("object", addObject);
		addition.addAttachment("attachment", new byte[0], "application/octet-stream");

		// Addition should override base (where applicable)
		base.merge(addition);
		assertThat(base.getSubstitution("boolean"), is(addBoolean));
		assertThat(base.getSubstitution("char"), is(addChar));
		assertThat(base.getSubstitution("long"), is(addLong));
		assertThat(base.getSubstitution("double"), is(addDouble));
		assertThat(base.getSubstitution("string"), is(addString));
		assertThat(base.getSubstitution("object"), is(baseObject));
		assertThat(base.getAttachments(), hasSize(1));

		// Addition should remain unchanged
		assertThat(addition.getSubstitution("boolean"), is(addBoolean));
		assertThat(addition.getSubstitution("char"), is(addChar));
		assertThat(addition.getSubstitution("long"), is(addLong));
		assertThat(addition.getSubstitution("double"), is(addDouble));
		assertThat(addition.getSubstitution("string"), is(addString));
		assertThat(addition.getSubstitution("object"), is(addObject));
		assertThat(base.getAttachments(), hasSize(1));

		// Object should have some substituted information
		TestSubstitutable subObject = (TestSubstitutable) base.getSubstitution("object");
		assertThat(subObject.name, is(addString));
		assertThat(subObject.dates, hasSize(2));
		assertThat(subObject.lookup.get("base"), is(baseString));
		assertThat(subObject.lookup.get("add"), is(addString));
		assertThat(subObject.lookup.get("shared"), is(addString));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testMergeCollectionSubstitution() {
		Notification base = new Notification("<test>");
		Notification addition = new Notification("<test>");

		String[] baseStringArray = new String[] { "This", "That" };
		Integer[] baseIntegerArray = new Integer[] { 1, 2, 3, 4 };
		String[] addStringArray = new String[] { "Other Thing" };
		List<String> baseStringCollection = Arrays.asList(baseStringArray);
		List<Integer> baseIntegerCollection = Arrays.asList(baseIntegerArray);
		List<String> addStringCollection = Arrays.asList(addStringArray);
		Object property1;
		Object property2;

		// Collection <- Collection
		base.addSubstitution("property1", new ArrayList<>(baseStringCollection));	// Pass in a copy
		base.addSubstitution("property2", new ArrayList<>(baseIntegerCollection));	// Pass in a copy
		addition.addSubstitution("property1", new ArrayList<>(addStringCollection));	// Pass in a copy
		addition.addSubstitution("property2", new ArrayList<>(addStringCollection));	// Pass in a copy

		base.merge(addition);
		property1 = base.getSubstitution("property1");
		property2 = base.getSubstitution("property2");
		assertThat(((Collection<?>) property1), hasSize(3));
		assertThat(((Collection<String>) property1), hasItem("Other Thing"));
		assertThat(property2, is(addStringCollection));

		// Collection <- Array
		base.addSubstitution("property1", new ArrayList<>(baseStringCollection));	// Pass in a copy
		base.addSubstitution("property2", new ArrayList<>(baseIntegerCollection));	// Pass in a copy
		addition.addSubstitution("property1", addStringArray);
		addition.addSubstitution("property2", addStringArray);

		base.merge(addition);
		property1 = base.getSubstitution("property1");
		property2 = base.getSubstitution("property2");
		assertThat(((Collection<?>) property1), hasSize(3));
		assertThat(((Collection<String>) property1), hasItem("Other Thing"));
		assertThat(property2, is(addStringArray));

		// Array <- Array
		base.addSubstitution("property1", baseStringArray);
		base.addSubstitution("property2", baseIntegerArray);
		addition.addSubstitution("property1", addStringArray);
		addition.addSubstitution("property2", addStringArray);

		base.merge(addition);
		property1 = base.getSubstitution("property1");
		property2 = base.getSubstitution("property2");
		assertThat(((String[]) property1), arrayWithSize(3));
		assertThat(((String[]) property1), hasItemInArray("Other Thing"));
		assertThat(property2, is(addStringArray));

		// Array <- Collection
		base.addSubstitution("property1", baseStringArray);
		base.addSubstitution("property2", new ArrayList<>(baseIntegerCollection));	// Pass in a copy
		addition.addSubstitution("property1", addStringArray);
		addition.addSubstitution("property2", new ArrayList<>(addStringCollection));	// Pass in a copy

		base.merge(addition);
		property1 = base.getSubstitution("property1");
		property2 = base.getSubstitution("property2");
		assertThat(((String[]) property1), arrayWithSize(3));
		assertThat(((String[]) property1), hasItemInArray("Other Thing"));
		assertThat(property2, is(addStringCollection));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testMergeMapSubstitution() {
		Notification base = new Notification("<test>");
		Notification addition = new Notification("<test>");

		boolean baseBoolean = true;
		char baseChar = 'b';
		long baseLong = 1234L;
		double baseDouble = 12.34;
		String baseString = "base";
		TestSubstitutable baseObject = new TestSubstitutable(1);
		String[] baseStringArray = new String[] { "This", "That" };
		// TCM: Note: need to copy into an ArrayList because Arrays$ArrayList is immutable
		List<String> baseStringCollection = new ArrayList<String>(Arrays.asList(baseStringArray));
		Map<String, Object> baseMap = new HashMap<>();
		baseMap.put("boolean", baseBoolean);
		baseMap.put("char", baseChar);
		baseMap.put("long", baseLong);
		baseMap.put("double", baseDouble);
		baseMap.put("string", baseString);
		baseMap.put("object", baseObject);
		baseMap.put("array", baseStringArray);
		baseMap.put("collection", baseStringCollection);
		base.addSubstitution("map", (Serializable) baseMap);

		boolean addBoolean = false;
		char addChar = 'a';
		long addLong = 4321L;
		double addDouble = 43.21;
		String addString = "addition";
		String[] addStringArray = new String[] { "Other Thing" };
		List<String> addStringCollection = new ArrayList<String>(Arrays.asList(addStringArray));
		Map<String, Object> addMap = new HashMap<>();
		addMap.put("boolean", addBoolean);
		addMap.put("char", addChar);
		addMap.put("long", addLong);
		addMap.put("double", addDouble);
		addMap.put("string", addString);
		addMap.put("array", addStringArray);
		addMap.put("collection", addStringCollection);
		addition.addSubstitution("map", (Serializable) addMap);

		base.merge(addition);
		Map<String, Object> mergeMap = (Map<String, Object>) base.getSubstitution("map");
		assertThat(mergeMap.get("boolean"), is(addBoolean));
		assertThat(mergeMap.get("char"), is(addChar));
		assertThat(mergeMap.get("long"), is(addLong));
		assertThat(mergeMap.get("double"), is(addDouble));
		assertThat(mergeMap.get("string"), is(addString));
		assertThat(mergeMap.get("object"), is(baseObject));

		String[] array = (String[]) mergeMap.get("array");
		assertThat(array, arrayWithSize(3));
		assertThat(array, hasItemInArray("Other Thing"));

		Collection<String> collection = (Collection<String>) mergeMap.get("collection");
		assertThat(collection, hasSize(3));
		assertThat(collection, hasItem("Other Thing"));
	}

	@Test
	public void testTemplate() throws NotificationException {
		Notification n = new Notification("echo.groovy");
		String subject = "Test Subject";
		String content = "Test Content";
		n.addSubstitution("_subject", subject);
		n.addSubstitution("_content", content);
		Map<?, ?> templateBindings = n.applyTemplate();
		assertThat((String) templateBindings.get("subject"), is(equalTo(subject)));
		assertThat(((GString) templateBindings.get("content")).toString(), is(equalTo(content)));
	}

	@Test
	public void testAttachment() throws IOException {
		Notification n = new Notification("<test>");
		byte[] docBytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("test-doc.pdf"));
		assertThat(docBytes.length, greaterThan(0));
		n.addAttachment("doc", docBytes, "application/pdf");
		assertThat(n.getAttachments(), is(not(empty())));
		assertThat(n.getAttachments().get(0).getData(), is(not(nullValue())));
	}

	@Test
	public void testEmbeddedImage() throws IOException {
		Notification n = new Notification("<test>");
		byte[] imageBytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("test-image.png"));
		assertThat(imageBytes.length, greaterThan(0));
		n.addEmbeddedImage("image", imageBytes, "image/png");
		assertThat(n.getEmbeddedImages(), is(not(empty())));
		assertThat(n.getEmbeddedImages().get(0).getData(), is(not(nullValue())));
	}
}
