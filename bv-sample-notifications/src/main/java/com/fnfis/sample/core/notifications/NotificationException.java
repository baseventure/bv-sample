package com.fnfis.sample.core.notifications;

public class NotificationException extends Exception {
	private static final long serialVersionUID = 7309968565124461595L;
	
	private boolean retryable = false;

	// CONSTRUCTORS
	
	public NotificationException() { super(); }
	public NotificationException(String message) { super(message); }
	public NotificationException(Throwable cause) { super(cause); }
	public NotificationException(String message, Throwable cause) { super(message, cause); }
	public NotificationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	// GETTERS/SETTERS
	
	public boolean isRetryable() { return retryable; }
	public void setRetryable(boolean retryable) { this.retryable = retryable; }
}
